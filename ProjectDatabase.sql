USE project;

CREATE TABLE Shoe (
     shoe_id NVARCHAR(150) NOT NULL,
     shoe_name NVARCHAR(150) NOT NULL,
     price int NOT NULL,
     shoe_type NVARCHAR(150) NOT NULL,
     shoe_brand NVARCHAR(150) NOT NULL,
     shoe_description NVARCHAR(500) NOT NULL,
     PRIMARY KEY (shoe_id)
);
INSERT INTO Shoe(shoe_id, shoe_name, price, shoe_type, shoe_brand, shoe_description) VALUES ('170285C', 'Converse Chuck 70 My Story', 1900000, 'Chuck 70', 'Converse', N'So với những siêu phẩm được ra mắt ở những năm trước, Converse Chuck 70 My Story được nhận định là sản phẩm có phần đơn giản, tinh tế mà không kém phần đẹp mắt cũng như vẫn truyền tải được thông điệp ý nghĩa như một món quà cho các fans');
INSERT INTO Shoe(shoe_id, shoe_name, price, shoe_type, shoe_brand, shoe_description) VALUES ('168816V', 'Converse Run Star Hike Twisted Classic Foundational Canvas', 2200000, 'Chuck Taylor', 'Converse', N'Giày Converse Run Star Hike hiện được ra mắt với hai tông màu chính là trắng basic và đen cá tính. Chắc chắn trong tương lai, Run Star Hike có thể tạo cho mình được một đế chế riêng không chỉ dành cho dân leo núi mà còn có chỗ đứng trong lòng các bạn trẻ Việt Nam');
INSERT INTO Shoe(shoe_id, shoe_name, price, shoe_type, shoe_brand, shoe_description) VALUES ('166800V', 'Converse Run Star Hike Twisted Classic Foundational Canvas', 2500000, 'Chuck Taylor', 'Converse', N'Sản phẩm được "remix" từ Chuck và Runner khi 2 yếu tố thời trang được thể hiện xen kẽ: Upper canvas và đế Run Star zig-zag. Đế giày Giày Converse Run Star Hike được thiết kế với dạng răng cưa to bản, giúp tăng độ bám một cách hiệu quả vừa tạo được điểm nhấn về phong cách và ấn tượng về thời trang.');
INSERT INTO Shoe(shoe_id, shoe_name, price, shoe_type, shoe_brand, shoe_description) VALUES ('VN0A5FCBOFW', 'Vans Old Skool Skate', 2200000, 'Old Skool', 'Vans', N'THÔNG TIN SẢN PHẨM Thương hiệu Vans Xuất xứ thương hiệu Mỹ Sản xuất tại Việt Nam ...');
INSERT INTO Shoe(shoe_id, shoe_name, price, shoe_type, shoe_brand, shoe_description) VALUES ('VN0A4BV518P', 'Vans Old Shool Pig Suede', 2200000, 'Old Skool', 'Vans', N'Phiên bản Vans Old Skool Pig Suede vẫn xuất hiện họa tiết lượn sóng “sidestripe V” đặc trưng, mang ý nghĩa biểu tượng của nhà Vans và được làm nổi bật với tông màu trắng hoặc ở phiên bản xanh rêu sọc jazz này sẽ cùng tông màu với thân giày');
INSERT INTO Shoe(shoe_id, shoe_name, price, shoe_type, shoe_brand, shoe_description) VALUES ('VN0A2Z5I18P', 'Vans Authentic Pig Suede', 2050000, 'Authentic', 'Vans', N'Phiên bản giày Vans Authentic Pig Suede gọn nhẹ, được tối ưu để cả các cô nàng có thể sử dụng, mix cùng những trang phục từ cá tính cho tới điệu đà đều phù hợp và đẹp mắt.');

INSERT INTO Shoe(shoe_id, shoe_name, price, shoe_type, shoe_brand, shoe_description) VALUES ('VN0A4BV4XEY', 'Vans Era Flame', 1550000, 'Era', 'Vans', N'Được ưa chuộng ở thị trường Mỹ vào những năm 1990, phiên bản giày Vans với họa tiết hình lửa hay còn gọi là giày Vans Flame đã chinh phục không biết bao nhiêu thế hệ người dùng.');
INSERT INTO Shoe(shoe_id, shoe_name, price, shoe_type, shoe_brand, shoe_description) VALUES ('VN0A38F7PHN', 'Vans Slip-On Flame', 1500000, 'Slip-On', 'Vans', N'Giày Vans Flame Slip-on cũng là một phiên bản được chú ý trong số những item lửa nổi bật của Vans');
INSERT INTO Shoe(shoe_id, shoe_name, price, shoe_type, shoe_brand, shoe_description) VALUES ('VN0A2XSBPHN', 'Vans Sk8-Hi Flame Reissue', 1900000, 'Sk8-Hi', 'Vans', N'Giày Vans Flame qua nhiều năm xuất hiện đã là bộ sưu tập chủ lực của Vans gồm 3 phiên bản giày đình đám Era, Slip-on và phiên bản giày cao cổ Sk8-Hi.');
INSERT INTO Shoe(shoe_id, shoe_name, price, shoe_type, shoe_brand, shoe_description) VALUES ('VN0A38G219Z', 'Vans Old Shool 36 DX Anaheim Factory Hoffman Fabrics', 2200000, 'Old Skool 36 Dx', 'Vans', N'Vans với nguồn gốc xuất phát từ Nam California, đã minh chứng cho việc nền móng của đầy lịch sử của thương hiệu đình đám của mình và tri ân nó bằng BST mang tên Vans Anaheim Factory Hoffman Fabrics');
INSERT INTO Shoe(shoe_id, shoe_name, price, shoe_type, shoe_brand, shoe_description) VALUES ('VN0A4U3B18Z', 'Vans Old Skool Pig Suede', 2200000, 'Old Skool', 'Vans', N'Phiên bản Vans Old Skool Pig Suede vẫn xuất hiện họa tiết lượn sóng “sidestripe V” đặc trưng, mang ý nghĩa biểu tượng của nhà Vans và được làm nổi bật với tông màu trắng hoặc ở phiên bản xanh rêu sọc jazz này sẽ cùng tông màu với thân giày');
INSERT INTO Shoe(shoe_id, shoe_name, price, shoe_type, shoe_brand, shoe_description) VALUES ('VN0A2Z5I18Z', 'Vans Authentic Pig Suede', 2050000, 'Authentic', 'Vans', N'Phiên bản giày Vans Authentic Pig Suede gọn nhẹ, được tối ưu để cả các cô nàng có thể sử dụng, mix cùng những trang phục từ cá tính cho tới điệu đà đều phù hợp và đẹp mắt.');

INSERT INTO Shoe(shoe_id, shoe_name, price, shoe_type, shoe_brand, shoe_description) VALUES ('569540C', 'Converse Chuck 70 Black Ice', 2100000, 'Chuck 70', 'Converse', N'BST Converse Chuck 70 Black Ice với mục đích ra mắt những dòng sản phẩm mang lại sự ấm cúng và trải nghiệm thoải mái nhất cho kỳ nghỉ lễ dài của các nước Châu Âu, Châu Mỹ và chào đón 2021');
INSERT INTO Shoe(shoe_id, shoe_name, price, shoe_type, shoe_brand, shoe_description) VALUES ('570452C', 'Converse Chuck Taylor All Star Platform Anodized Metals', 2050000, 'Chuck Taylor All Star ', 'Converse', N'Với vóc dáng hoàn hảo và cực kỳ thể thao, siêu phẩm của nhà Converse mang tên giày Converse Platform Anodized Metals mang một nét đẹp siêu hiện đại, trẻ trung và xứng đáng để các bạn trẻ mê mẩn ngay từ lần đầu tiên nhìn thấy');
INSERT INTO Shoe(shoe_id, shoe_name, price, shoe_type, shoe_brand, shoe_description) VALUES ('170108C', 'Converse Chuck Taylor All Star Wordmark High Street', 1600000, 'Chuck Taylor All Star ', 'Converse', N'THÔNG TIN SẢN PHẨM Thương hiệu Converse Xuất xứ thương hiệu Mỹ Sản xuất tại Việt Nam ...');
INSERT INTO Shoe(shoe_id, shoe_name, price, shoe_type, shoe_brand, shoe_description) VALUES ('571119C', 'Converse Chuck Taylor All Star Platform Valentine Day', 1800000, 'Platform', 'Converse', N'Thêm một sự lựa chọn mới mẻ có phần tinh tế - thuần khiết và đẳng cấp hơn cho các bạn trẻ lựa chọn để sử dụng hay làm quà tặng trong ngày lễ tình nhân 14.2 sắp tới đó chính là iteam mang tên giày Converse Platform Valentine Day High Top. Thiết kế truyền thống, cổ điển với phần đế Platform siêu dày dặn chắc chắn sẽ chiều lòng được từ các bạn nam cá tính cho tới các bạn nữ điệu đà.');
INSERT INTO Shoe(shoe_id, shoe_name, price, shoe_type, shoe_brand, shoe_description) VALUES ('171118C', 'Converse Chuck 70 Valentine Day', 2000000, '1970s', 'Converse', N'Thể hiện tình yêu nồng nhiệt với Converse Chuck 70 Valentine Day High Top. Tình yêu vốn nồng nhiệt, đôi khi ngông cuồng và máu lửa - đó chính là những gia vị khiến các bạn cảm nhận được cuộc sống này tươi đẹp hơn. Không để các bạn phải chờ lâu, Converse đã cực kỳ nhanh chóng tung ra dòng sản phẩm dành riêng cho ngày lễ tình nhân');
INSERT INTO Shoe(shoe_id, shoe_name, price, shoe_type, shoe_brand, shoe_description) VALUES ('171117C', 'Converse Chuck 70 Valentine Day', 2000000, '1970s', 'Converse', N'Nếu như những đôi Chuck Taylor All Star xuất hiện với nét truyền thống, tinh tế, đơn giản và thể hiện được sự đáng quý, trân trọng dài lâu trong tình yêu, thì siêu phẩm Converse Chuck 70 Valentine Day High Top này lại toát ra được tình cảm nồng cháy, trẻ trung như muốn “thiêu đốt”. Dòng sản phẩm này phù hợp với những bạn trẻ mới yêu, đang yêu hoặc đang muốn khẳng định một tình yêu dài lâu - chắc chắn - nồng cháy và cuồng nhiệt.');


CREATE TABLE Customer (
     customer_id int NOT NULL identity(1,1),
     customer_name NVARCHAR(150) NOT NULL,
     phone_number NVARCHAR(150) NOT NULL,
     customer_email NVARCHAR(150) NOT NULL,
     customer_address NVARCHAR(500) NOT NULL,
     customer_username NVARCHAR(150) NOT NULL,
     customer_password NVARCHAR(150) NOT NULL,
     PRIMARY KEY (customer_id)
);
INSERT INTO Customer(customer_name, phone_number, customer_email, customer_address, customer_username, customer_password) VALUES (N'Nguyen The Hai', '0123456789', 'abc@gmailc.om', 'Dai hoc FPT, Ha Noi', 'hai', '123456');


CREATE TABLE Orders (
     order_id int NOT NULL,
     customer_id int,
     shoe_id NVARCHAR(150) NOT NULL,
     size float NOT NULL,
     quantity int NOT NULL,
     price int NOT NULL,
     total int NOT NULL,
     order_name NVARCHAR(150) NOT NULL,
     order_address NVARCHAR(500) NOT NULL,
     order_phone NVARCHAR(150) NOT NULL,
     order_email NVARCHAR(150) NOT NULL,
     order_date Date NOT NULL,
     FOREIGN KEY (customer_id)
	 REFERENCES Customer (customer_id),
	 FOREIGN KEY (shoe_id)
     REFERENCES Shoe (shoe_id)
);


CREATE TABLE productImage (
     shoe_id NVARCHAR(150) NOT NULL,
     shoe_image1 NVARCHAR(150) NOT NULL,
     shoe_image2 NVARCHAR(150) NOT NULL,
     shoe_image3 NVARCHAR(150) NOT NULL,
     shoe_image4 NVARCHAR(150) NOT NULL,
     PRIMARY KEY (shoe_id),
     FOREIGN KEY (shoe_id)
     REFERENCES Shoe (shoe_id)
);
INSERT INTO productImage(shoe_id, shoe_image1, shoe_image2, shoe_image3, shoe_image4) VALUES ('170285C', 'https://bizweb.dktcdn.net/thumb/1024x1024/100/347/923/products/170285c-5.jpg', 'https://bizweb.dktcdn.net/100/347/923/products/170285c-1.jpg?v=1613623468667', 'https://bizweb.dktcdn.net/100/347/923/products/170285c-2.jpg?v=1613623468667', 'https://bizweb.dktcdn.net/100/347/923/products/170285c-4.jpg?v=1613623468667');
INSERT INTO productImage(shoe_id, shoe_image1, shoe_image2, shoe_image3, shoe_image4) VALUES ('168816V', 'https://bizweb.dktcdn.net/100/347/923/products/168816v-3.jpg?v=1609400063423', 'https://bizweb.dktcdn.net/thumb/1024x1024/100/347/923/products/168816v-2.jpg?v=1609400068200', 'https://bizweb.dktcdn.net/thumb/1024x1024/100/347/923/products/168816v-1.jpg?v=1609400071757', 'https://bizweb.dktcdn.net/100/347/923/products/168816v-6.jpg?v=1609400075277');
INSERT INTO productImage(shoe_id, shoe_image1, shoe_image2, shoe_image3, shoe_image4) VALUES ('166800V', 'https://bizweb.dktcdn.net/thumb/1024x1024/100/347/923/products/166800v-6.jpg?v=1594306260957', 'https://bizweb.dktcdn.net/100/347/923/products/166800v-4.jpg?v=1594306260957', 'https://bizweb.dktcdn.net/100/347/923/products/166800v-3.jpg?v=1594306260957', 'https://bizweb.dktcdn.net/100/347/923/products/166800v-2.jpg?v=1594306260957');
INSERT INTO productImage(shoe_id, shoe_image1, shoe_image2, shoe_image3, shoe_image4) VALUES ('VN0A5FCBOFW', '//bizweb.dktcdn.net/thumb/1024x1024/100/347/923/products/vn0a5fcbofw-8.jpg', 'https://bizweb.dktcdn.net/100/347/923/products/vn0a5fcbofw-9.jpg?v=1614678925733', 'https://bizweb.dktcdn.net/100/347/923/products/vn0a5fcbofw-1.jpg?v=1614678928513', 'https://bizweb.dktcdn.net/100/347/923/products/vn0a5fcbofw-2.jpg?v=1614678931513');
INSERT INTO productImage(shoe_id, shoe_image1, shoe_image2, shoe_image3, shoe_image4) VALUES ('VN0A2Z5I18P', '//bizweb.dktcdn.net/thumb/1024x1024/100/347/923/products/vn0a2z5i18p-4.jpg', 'https://bizweb.dktcdn.net/100/347/923/products/vn0a2z5i18p-1.jpg?v=1611921098613', 'https://bizweb.dktcdn.net/100/347/923/products/vn0a2z5i18p-2.jpg?v=1611921098613', 'https://bizweb.dktcdn.net/100/347/923/products/vn0a2z5i18p-3.jpg?v=1611921098613');
INSERT INTO productImage(shoe_id, shoe_image1, shoe_image2, shoe_image3, shoe_image4) VALUES ('VN0A4BV518P', '//bizweb.dktcdn.net/thumb/1024x1024/100/347/923/products/vn0a4bv518p-3.jpg?v=1611917847197', 'https://bizweb.dktcdn.net/100/347/923/products/vn0a4bv518p-2.jpg?v=1611917847197', 'https://bizweb.dktcdn.net/100/347/923/products/vn0a4bv518p-1-2e5f8028-8d9d-4b9f-abc1-25c8d1f1126a.jpg?v=1611917847197', 'https://bizweb.dktcdn.net/100/347/923/products/vn0a4bv518p-5.jpg?v=1611917853827');

INSERT INTO productImage(shoe_id, shoe_image1, shoe_image2, shoe_image3, shoe_image4) VALUES ('VN0A4BV4XEY', 'https://bizweb.dktcdn.net/thumb/1024x1024/100/347/923/products/vn0a4bv4xey-4.jpg', 'https://bizweb.dktcdn.net/100/347/923/products/vn0a4bv4xey-2.jpg?v=1606563552050', 'https://bizweb.dktcdn.net/100/347/923/products/vn0a4bv4xey-3.jpg?v=1606563555523', 'https://bizweb.dktcdn.net/100/347/923/products/vn0a4bv4xey-1.jpg?v=1606563558947');
INSERT INTO productImage(shoe_id, shoe_image1, shoe_image2, shoe_image3, shoe_image4) VALUES ('VN0A38F7PHN', '//bizweb.dktcdn.net/thumb/1024x1024/100/347/923/products/vn0a38f7phn-5.jpg?v=1606564282733', 'https://bizweb.dktcdn.net/100/347/923/products/vn0a38f7phn-3.jpg?v=1606564287057', 'https://bizweb.dktcdn.net/100/347/923/products/vn0a38f7phn-1.jpg?v=1606564291513', 'https://bizweb.dktcdn.net/100/347/923/products/vn0a38f7phn-4.jpg?v=1606564296137');
INSERT INTO productImage(shoe_id, shoe_image1, shoe_image2, shoe_image3, shoe_image4) VALUES ('VN0A2XSBPHN', '//bizweb.dktcdn.net/thumb/1024x1024/100/347/923/products/vn0a2xsbphn-4.jpg?v=1606561176130', 'https://bizweb.dktcdn.net/100/347/923/products/vn0a2xsbphn-3.jpg?v=1606561179493', 'https://bizweb.dktcdn.net/100/347/923/products/vn0a2xsbphn-2.jpg?v=1606561182700', 'https://bizweb.dktcdn.net/100/347/923/products/vn0a2xsbphn-1.jpg?v=1606561186150');
INSERT INTO productImage(shoe_id, shoe_image1, shoe_image2, shoe_image3, shoe_image4) VALUES ('VN0A38G219Z', '//bizweb.dktcdn.net/thumb/1024x1024/100/347/923/products/vn0a38g219z-7.jpg?v=1611926994217', 'https://bizweb.dktcdn.net/100/347/923/products/vn0a38g219z-8.jpg?v=1611926994217', 'https://bizweb.dktcdn.net/100/347/923/products/vn0a38g219z-4-682d6464-ad7b-417f-adae-046147d40d30.jpg?v=1611926994217', 'https://bizweb.dktcdn.net/100/347/923/products/vn0a38g219z-5-a9ccfee9-2676-4e58-9d48-76375bf966d7.jpg?v=1611926994217');
INSERT INTO productImage(shoe_id, shoe_image1, shoe_image2, shoe_image3, shoe_image4) VALUES ('VN0A4U3B18Z', '//bizweb.dktcdn.net/thumb/1024x1024/100/347/923/products/vn0a4u3b18z-2.jpg?v=1611921183667', 'https://bizweb.dktcdn.net/100/347/923/products/vn0a4u3b18z-1.jpg?v=1611921183667', 'https://bizweb.dktcdn.net/100/347/923/products/vn0a4u3b18z-4.jpg?v=1611921183667', 'https://bizweb.dktcdn.net/100/347/923/products/vn0a4u3b18z-5.jpg?v=1611921183667');
INSERT INTO productImage(shoe_id, shoe_image1, shoe_image2, shoe_image3, shoe_image4) VALUES ('VN0A2Z5I18Z', '//bizweb.dktcdn.net/thumb/1024x1024/100/347/923/products/vn0a2z5i18z-6.jpg?v=1611921892117', 'https://bizweb.dktcdn.net/100/347/923/products/vn0a2z5i18z-4.jpg?v=1611921892117', 'https://bizweb.dktcdn.net/100/347/923/products/vn0a2z5i18z-3.jpg?v=1611921892117', 'https://bizweb.dktcdn.net/100/347/923/products/vn0a2z5i18z-2-198e130c-514d-4c02-96ac-b88c559779a7.jpg?v=1611921892117');

INSERT INTO productImage(shoe_id, shoe_image1, shoe_image2, shoe_image3, shoe_image4) VALUES ('569540C', '//bizweb.dktcdn.net/thumb/1024x1024/100/347/923/products/569540c-5.jpg?v=1607416140147', 'https://bizweb.dktcdn.net/100/347/923/products/569540c-4.jpg?v=1607416142603', 'https://bizweb.dktcdn.net/100/347/923/products/569540c-2.jpg?v=1607416145687', 'https://bizweb.dktcdn.net/100/347/923/products/569540c-3.jpg?v=1607416148000');
INSERT INTO productImage(shoe_id, shoe_image1, shoe_image2, shoe_image3, shoe_image4) VALUES ('570452C', '//bizweb.dktcdn.net/thumb/1024x1024/100/347/923/products/570452c-7.jpg?v=1610194914047', 'https://bizweb.dktcdn.net/100/347/923/products/570452c-1.jpg?v=1610194914047', 'https://bizweb.dktcdn.net/100/347/923/products/570452c-3.jpg?v=1610194914047', 'https://bizweb.dktcdn.net/100/347/923/products/570452c-2.jpg?v=1610194914047');
INSERT INTO productImage(shoe_id, shoe_image1, shoe_image2, shoe_image3, shoe_image4) VALUES ('170108C', '//bizweb.dktcdn.net/thumb/1024x1024/100/347/923/products/170108c-4.jpg?v=1611233738410', 'https://bizweb.dktcdn.net/100/347/923/products/170108c-1.jpg?v=1611233741583', 'https://bizweb.dktcdn.net/100/347/923/products/170108c-3.jpg?v=1611233744903', 'https://bizweb.dktcdn.net/100/347/923/products/170108c-6.jpg?v=1611233752220');
INSERT INTO productImage(shoe_id, shoe_image1, shoe_image2, shoe_image3, shoe_image4) VALUES ('571119C', '//bizweb.dktcdn.net/thumb/1024x1024/100/347/923/products/571119c-5-ec4b8a86-c6b3-47b0-9783-93c26bf2afac.jpg?v=1611830229403', 'https://bizweb.dktcdn.net/100/347/923/products/571119c-1-8949e7a6-d87e-4d04-85c6-7b33ff9971f9.jpg?v=1611830233147', 'https://bizweb.dktcdn.net/100/347/923/products/571119c-6.jpg?v=1611830236280', 'https://bizweb.dktcdn.net/100/347/923/products/571119c-2-48b3ef36-2bbb-4f1b-97e8-f778fc4648bf.jpg?v=1611830243863');
INSERT INTO productImage(shoe_id, shoe_image1, shoe_image2, shoe_image3, shoe_image4) VALUES ('171118C', '//bizweb.dktcdn.net/thumb/1024x1024/100/347/923/products/171118c-8.jpg?v=1611923306753', 'https://bizweb.dktcdn.net/100/347/923/products/171118c-5-1ff929ff-ae83-4f13-84cf-21d548c6f80b.jpg?v=1611923306753', 'https://bizweb.dktcdn.net/100/347/923/products/171118c-4.jpg?v=1611923306753', 'https://bizweb.dktcdn.net/100/347/923/products/171118c-1.jpg?v=1611923306753');
INSERT INTO productImage(shoe_id, shoe_image1, shoe_image2, shoe_image3, shoe_image4) VALUES ('171117C', '//bizweb.dktcdn.net/thumb/1024x1024/100/347/923/products/171117c-8.jpg?v=1611923741113', 'https://bizweb.dktcdn.net/100/347/923/products/171117c-5-0682439e-dd0a-4e3d-9d78-06119c185d6e.jpg?v=1611923741113', 'https://bizweb.dktcdn.net/100/347/923/products/171117c-4.jpg?v=1611923741113', 'https://bizweb.dktcdn.net/100/347/923/products/171117c-1.jpg?v=1611923741113');


CREATE TABLE Quantity (
     shoe_id NVARCHAR(150) NOT NULL,
     size7 int NOT NULL,
     size75 int NOT NULL,
     size8 int NOT NULL,
     size85 int NOT NULL,
     size9 int NOT NULL,
     size95 int NOT NULL,
     PRIMARY KEY (shoe_id),
     FOREIGN KEY (shoe_id)
     REFERENCES Shoe (shoe_id)
);
INSERT INTO Quantity(shoe_id, size7, size75, size8, size85, size9, size95) VALUES ('170285C', 50, 0, 50, 50, 50, 0);
INSERT INTO Quantity(shoe_id, size7, size75, size8, size85, size9, size95) VALUES ('168816V', 50, 50, 50, 50, 50, 50);
INSERT INTO Quantity(shoe_id, size7, size75, size8, size85, size9, size95) VALUES ('166800V', 50, 50, 50, 50, 50, 50);
INSERT INTO Quantity(shoe_id, size7, size75, size8, size85, size9, size95) VALUES ('VN0A5FCBOFW', 50, 50, 50, 50, 50, 50);
INSERT INTO Quantity(shoe_id, size7, size75, size8, size85, size9, size95) VALUES ('VN0A2Z5I18P', 50, 50, 50, 50, 50, 50);
INSERT INTO Quantity(shoe_id, size7, size75, size8, size85, size9, size95) VALUES ('VN0A4BV518P', 50, 50, 50, 50, 50, 50);

INSERT INTO Quantity(shoe_id, size7, size75, size8, size85, size9, size95) VALUES ('VN0A4BV4XEY', 50, 50, 50, 50, 50, 50);
INSERT INTO Quantity(shoe_id, size7, size75, size8, size85, size9, size95) VALUES ('VN0A38F7PHN', 50, 50, 50, 50, 50, 50);
INSERT INTO Quantity(shoe_id, size7, size75, size8, size85, size9, size95) VALUES ('VN0A2XSBPHN', 50, 50, 50, 50, 50, 50);
INSERT INTO Quantity(shoe_id, size7, size75, size8, size85, size9, size95) VALUES ('VN0A38G219Z', 50, 50, 50, 50, 50, 50);
INSERT INTO Quantity(shoe_id, size7, size75, size8, size85, size9, size95) VALUES ('VN0A4U3B18Z', 50, 50, 50, 50, 50, 50);
INSERT INTO Quantity(shoe_id, size7, size75, size8, size85, size9, size95) VALUES ('VN0A2Z5I18Z', 50, 50, 50, 50, 50, 50);

INSERT INTO Quantity(shoe_id, size7, size75, size8, size85, size9, size95) VALUES ('569540C', 50, 50, 50, 50, 50, 50);
INSERT INTO Quantity(shoe_id, size7, size75, size8, size85, size9, size95) VALUES ('570452C', 50, 50, 50, 50, 50, 50);
INSERT INTO Quantity(shoe_id, size7, size75, size8, size85, size9, size95) VALUES ('170108C', 50, 50, 50, 50, 50, 50);
INSERT INTO Quantity(shoe_id, size7, size75, size8, size85, size9, size95) VALUES ('571119C', 50, 50, 50, 50, 50, 50);
INSERT INTO Quantity(shoe_id, size7, size75, size8, size85, size9, size95) VALUES ('171118C', 50, 50, 50, 50, 50, 50);
INSERT INTO Quantity(shoe_id, size7, size75, size8, size85, size9, size95) VALUES ('171117C', 50, 50, 50, 50, 50, 50);



CREATE TABLE Admin (
     admin_id int NOT NULL identity(1,1),
     admin_name NVARCHAR(150) NOT NULL,
     admin_password NVARCHAR(150) NOT NULL,
     PRIMARY KEY (admin_id),
);
INSERT INTO Admin(admin_name, admin_password) VALUES ('admin', 'admin');