<%-- 
    Document   : detail
    Created on : Mar 23, 2021, 3:09:07 PM
    Author     : nguye
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="DAL.ShoeDAO"%>
<%@page import="model.Shoe"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Detail Products - SneakersStore</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
    <%
        Shoe shoe = (Shoe) request.getAttribute("shoe");
    %>
</head>
<body>
    <div class="header-et">
        <div class="container-et">
            <div class="navbar-et">
                <div class="logo">
                    <a href="index.html">
                        <img src="Image/Logo_2.png" width="150px">
                    </a>
                </div>
                <nav class="nav-et">
                    <ul id="MenuItems">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="product.jsp?page=1">Product</a></li>
                        <li><a href="account.jsp">Account</a></li>
                    </ul>
                </nav>
                <a href="cart.jsp">
                    <img src="Image/cart.jpg" width="30px" height="30px">
                </a>
                <img src="Image/menu.png" class="menu-icon-et" onclick="menutoggleet()">
            </div>
        </div> 
    </div>

    <%String[] shoe_image = shoe.getShoe_image();%>

    <!-- -----------Single product detail------- -->
    <div class="small-container-et single-product">
        <div class="row-et">
            <div class="col-2-et">
                <img src=<%=shoe_image[0]%> width="100%" id="ProductImg">

                <div class="small-img-row-et">
                    <div class="small-img-col-et">
                        <img src=<%=shoe_image[0]%> width="100%" class="small-img-et">
                    </div>
                    <div class="small-img-col-et">
                        <img src=<%=shoe_image[1]%> width="100%" class="small-img-et">
                    </div>
                    <div class="small-img-col-et">
                        <img src=<%=shoe_image[2]%> width="100%" class="small-img-et">
                    </div>
                    <div class="small-img-col-et">
                        <img src=<%=shoe_image[3]%> width="100%" class="small-img-et">
                    </div>
                </div>

            </div>
            <div class="col-2-et">
                <p>Home / <%=shoe.getShoe_brand()%></p>
                <h1><%=shoe.getShoe_name()%></h1>
                <h4><%=shoe.getPrice()%>₫</h4>
                <form name="add" action="AddtoCart" method="GET">
                    <input type="hidden" value="<%=shoe.getShoe_id()%>" name="shoe_id"/>
                    <select name="selectSize">
                        <%if (shoe.getQuantity().getSize7() != 0) {%>
                        <option value="7">7</option>
                        <%}%>
                        <%if (shoe.getQuantity().getSize75() != 0) {%>
                        <option value="7.5">7.5</option>
                        <%}%>
                        <%if (shoe.getQuantity().getSize8() != 0) {%>
                        <option value="8">8</option>
                        <%}%>
                        <%if (shoe.getQuantity().getSize85() != 0) {%>
                        <option value="8.5">8.5</option>
                        <%}%>
                        <%if (shoe.getQuantity().getSize9() != 0) {%>
                        <option value="9">9</option>
                        <%}%>
                        <%if (shoe.getQuantity().getSize95() != 0) {%>
                        <option value="9.5">9.5</option>
                        <%}%>
                    </select>
                    <input type="number" value="1" name="quantity">
                    <input type="submit" value="Add to Cart" class="btn-et"/>
                </form>
                <h3>Product Details <i class="fa fa-indent"></i></h3>
                <br>
                <p><%=shoe.getShoe_description()%></p>
            </div>
        </div>
    </div>

    <!-- -------- title---------- -->
    <div class="small-container-et">
        <div class="row-et row-2">
            <h2>Related Product</h2>
            <p>View more</p>
        </div>
    </div>

    <div class="small-container-et">
        <div class="row-et">
            <% ShoeDAO db = new ShoeDAO();
                ArrayList<Shoe> shoes = db.getShoeSameBrand(shoe.getShoe_brand());
                int size = 4;
                if (shoes.size() <= 4) {
                    size = shoes.size();
                }
                for (int i = 0; i < size; i++) {%>
            <div class="col-4-et">
                <a href="detail?id=<%=shoes.get(i).getShoe_id()%>">
                    <img src="<%= shoes.get(i).getShoe_image()[0]%>">
                </a>
                <a href="detail?id=<%=shoes.get(i).getShoe_id()%>" style="text-decoration: none">
                    <h4><%= shoes.get(i).getShoe_name()%></h4>
                </a>
                <div class="rating">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
                <p><%= shoes.get(i).getPrice()%>₫</p>
            </div>
            <%}%>
        </div>
    </div>
    <div class="footer-et">
        <div class="container-et">
            <div class="row-et">
                <div class="footer-et-col-2">
                    <img src="Image/Logo_2_resize.png">
                </div>
                <div class="footer-et-col-4">
                    <h3>Follow us</h3>
                    <ul>
                        <li>Facebook</li>
                        <li>Twitter</li>
                        <li>Instagram</li>
                        <li>Youtube</li>
                    </ul>
                </div>
            </div>
            <hr>
            <p class="Copyright">Copyright 2020 - SneakersStore</p>
        </div>
    </div>

    <script>
        var MenuItems = document.getElementById("MenuItems");
        MenuItems.style.maxHeight = "0px";
        function menutoggleet() {
            if (MenuItems.style.maxHeight == "0px") {
                MenuItems.style.maxHeight = "200px";
            } else {
                MenuItems.style.maxHeight = "0px";
            }
        }
    </script>

    <!-- ------------ js for product gallery ------------ -->

    <script>
        var ProductImg = document.getElementById("ProductImg");
        var smallImg = document.getElementsByClassName("small-img-et")
        smallImg[0].onclick = function () {
            ProductImg.src = smallImg[0].src
        }
        smallImg[1].onclick = function () {
            ProductImg.src = smallImg[1].src
        }
        smallImg[2].onclick = function () {
            ProductImg.src = smallImg[2].src
        }
        smallImg[3].onclick = function () {
            ProductImg.src = smallImg[3].src
        }
    </script>

</body>
</html>
