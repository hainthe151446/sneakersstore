<%-- 
    Document   : checkout
    Created on : Mar 28, 2021, 7:58:52 PM
    Author     : nguye
--%>

<%@page import="model.Customer"%>
<%@page import="java.util.List"%>
<%@page import="model.Cart"%>
<%@page import="model.Item"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Detail Products - SneakersStore</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
    <% session = request.getSession();
        Customer customer = (Customer) session.getAttribute("customer"); %>
</head>

<body class="grey lighten-3">


    <div class="header-et">
        <div class="container-et">
            <div class="navbar-et">
                <div class="logo">
                    <a href="index.html">
                        <img src="Image/Logo_2.png" width="150px">
                    </a>
                </div>
                <nav class="nav-et">
                    <ul id="MenuItems">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="product.jsp?page=1">Product</a></li>
                        <li><a href="account.jsp">Account</a></li>
                    </ul>
                </nav>
                <a href="cart.jsp">
                    <img src="Image/cart.jpg" width="30px" height="30px">
                </a>
                <img src="Image/menu.png" class="menu-icon-et" onclick="menutoggleet()">
            </div>
        </div> 
    </div>

    <main class="mt-5 pt-4">
        <div class="container wow fadeIn">
            <h2 class="my-5 h2 text-center">Checkout form</h2>
            <div class="row">
                <div class="col-md-8 mb-4">
                    <div class="card">
                        <form class="card-body" action="addOrder" method="POST">
                            <%if (customer != null) {%>
                            <div class="col-md-6 mb-4">
                                <div class="md-form">
                                    <label for="firstName" class="">Full name</label>
                                    <input type="text" id="name" class="form-control" value="<%=customer.getName()%>" name="name">
                                </div>
                            </div>
                            <div class="col-md-6 mb-4">
                                <div class="md-form">
                                    <label for="email" class="">Email</label>
                                    <input type="text" id="email" class="form-control" value="<%=customer.getEmail()%>" name="email">
                                </div>
                            </div>
                            <div class="col-md-6 mb-4">
                                <div class="md-form">
                                    <label for="phone" class="">Phone</label>
                                    <input type="text" id="phone" class="form-control" value="<%=customer.getPhone()%>" name="phone">
                                </div>
                            </div>
                            <div class="md-form mb-5">
                                <label for="address" class="">Address</label>
                                <input
                                    type="text" id="address" class="form-control" value="<%=customer.getAddress()%>" name="address">
                            </div>
                            <%} else {%>
                            <div class="col-md-6 mb-4">
                                <div class="md-form">
                                    <label for="firstName" class="">Full name</label>
                                    <input type="text" id="name" class="form-control" placeholder="Full Name" name="name">
                                </div>
                            </div>
                            <div class="col-md-6 mb-4">
                                <div class="md-form">
                                    <label for="email" class="">Email</label>
                                    <input type="text" id="email" class="form-control" placeholder="youremail@example.com" name="email">
                                </div>
                            </div>
                            <div class="col-md-6 mb-4">
                                <div class="md-form">
                                    <label for="phone" class="">Phone</label>
                                    <input type="text" id="phone" class="form-control" placeholder="Your phone" name="phone">
                                </div>
                            </div>
                            <div class="md-form mb-5">
                                <label for="address" class="">Address</label>
                                <input
                                    type="text" id="address" class="form-control" placeholder="1234 Main St" name="address">
                            </div>
                            <%}%>
                            <div class="md-form mb-5">
                                <label>Shipping: Ship COD</label>
                            </div>
                            <hr class="mb-4">

                            <button class="btn-et" type="submit" style="width: 50%">Continue to checkout</button>
                        </form>
                    </div>
                </div>

                <div class="col-md-4 mb-4">
                    <h4 class="d-flex justify-content-between align-items-center mb-3">
                        <span class="text-muted">Your cart</span>
                    </h4>
                    <ul class="list-group mb-3 z-depth-1">
                        <%  session = request.getSession();
                            Cart cart = (Cart) session.getAttribute("cart");
                            List<Item> items = cart.getItems();
                            int total = Integer.parseInt(String.valueOf(session.getAttribute("total")));
                            for (Item i : items) {%>
                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <div>
                                <h6 class="my-0"><%=i.getShoe().getShoe_name()%></h6>
                                <small class="text-muted"><%=i.getQuantity()%> x <%=i.getPrice()%>₫</small>
                            </div>
                            <span class="text-muted"><%=i.getQuantity() * i.getPrice()%>₫</span>
                        </li>
                        <%}%>

                        <li class="list-group-item d-flex justify-content-between">
                            <span>Total (VND)</span>
                            <strong><%=total%>₫</strong>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </main>

    <!-- ---------- footer -----------    -->
    <div class="footer-et">
        <div class="container-et">
            <div class="row-et">
                <div class="footer-et-col-2">
                    <img src="Image/Logo_2_resize.png">
                </div>
                <div class="footer-et-col-4">
                    <h3>Follow us</h3>
                    <ul>
                        <li>Facebook</li>
                        <li>Twitter</li>
                        <li>Instagram</li>
                        <li>Youtube</li>
                    </ul>
                </div>
            </div>
            <hr>
            <p class="Copyright">Copyright 2020 - SneakersStore</p>
        </div>
    </div>

    <script>
        var MenuItems = document.getElementById("MenuItems");
        MenuItems.style.maxHeight = "0px";
        function menutoggleet() {
            if (MenuItems.style.maxHeight == "0px") {
                MenuItems.style.maxHeight = "200px";
            } else {
                MenuItems.style.maxHeight = "0px";
            }
        }
    </script>
</body>
</html>