<%-- 
    Document   : order_edit
    Created on : Mar 29, 2021, 11:19:05 PM
    Author     : nguye
--%>

<%@page import="model.Order"%>
<%@page import="DAL.OrderDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>SneakersStore</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <link rel="stylesheet" href="style.css">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>

    </head>
    <body>
        <div class="header-et">
            <div class="container-et">
                <div class="navbar-et">
                    <div class="logo">
                        <a href="index.html">
                            <img src="Image/Logo_2.png" width="150px">
                        </a>
                    </div>
                    <nav class="nav-et">
                        <ul id="MenuItems">
                            <li><a href="index.html">Home</a></li>
                            <li><a href="product.jsp?page=1">Product</a></li>
                            <li><a href="account.jsp">Account</a></li>
                        </ul>
                    </nav>
                    <a href="cart.jsp">
                        <img src="Image/cart.jpg" width="30px" height="30px">
                    </a>
                    <img src="Image/menu.png" class="menu-icon-et" onclick="menutoggleet()">
                </div>
            </div>
        </div>

        <div class="small-container-et" style="padding: 5%">
            <%
                int function = Integer.parseInt(String.valueOf(request.getAttribute("function")));
                if (function == 1) {
            %>
            <form action="add?target=3" method="POST">
                Order ID <input type="text" name="order_id" />
                <br/>
                Customer ID <input type="text" name="customer_id" />
                <br/>
                Shoe ID <input type="text" name="shoe_id" />
                <br/>
                Size <input type="text" name="size" />
                <br/>
                Quantity <input type="text" name="quantity" />
                <br/>
                Price <input type="text" name="price" />
                <br/>      
                Total <input type="text" name="total" />
                <br/>    
                Name <input type="text" name="name" />
                <br/>    
                Address <input type="text" name="address" />
                <br/>
                Phone <input type="text" name="phone" />
                <br/>
                Email <input type="text" name="email" />
                <br/>
                Date <input type="text" name="date" />
                <br/>
                <input type="submit" value="Add"/>
            </form>
            <%} else if (function == 2) {
                int id = Integer.parseInt(String.valueOf(request.getAttribute("order_id")));
                String shoe_id = String.valueOf(request.getAttribute("shoe_id"));
                Float size = Float.parseFloat(String.valueOf(request.getAttribute("size")));
                OrderDAO db = new OrderDAO();
                Order order = db.getOrderbyId(id, shoe_id, size);
            %>
            <form action="edit?target=3&id=<%=order.getId()%>&oldshoe_id=<%=shoe_id%>&oldsize=<%=size%>" method="POST">
                Customer ID <input type="text" name="customer_id" value="<%=order.getCustomer_id()%>"/>
                <br/>
                Shoe ID <input type="text" name="shoe_id" value="<%=order.getShoe_id()%>"/>
                <br/>
                Size <input type="text" name="size" value="<%=order.getSize()%>"/>
                <br/>
                Quantity <input type="text" name="quantity" value="<%=order.getQuantity()%>"/>
                <br/>
                Price <input type="text" name="price" value="<%=order.getPrice()%>"/>
                <br/>      
                Total <input type="text" name="total" value="<%=order.getTotal()%>"/>
                <br/>    
                Name <input type="text" name="name" value="<%=order.getName()%>"/>
                <br/>    
                Address <input type="text" name="address" value="<%=order.getAddress()%>"/>
                <br/>
                Phone <input type="text" name="phone" value="<%=order.getPhone()%>"/>
                <br/>
                Email <input type="text" name="email" value="<%=order.getEmail()%>"/>
                <br/>
                Date <input type="text" name="date" value="<%=order.getDate()%>"/>
                <br/>
                <input type="submit" value="Save"/>
            </form>
            <%} else if (function == 3) {
                int id = Integer.parseInt(String.valueOf(request.getAttribute("order_id")));
                String shoe_id = String.valueOf(request.getAttribute("shoe_id"));
                Float size = Float.parseFloat(String.valueOf(request.getAttribute("size")));
                OrderDAO db = new OrderDAO();
                Order order = db.getOrderbyId(id, shoe_id, size);%>
            Customer ID <input type="text" name="customer_id" value="<%=order.getCustomer_id()%>"/>
            <br/>
            Shoe ID: <%=order.getShoe_id()%>
            <br/>
            Size: <%=order.getSize()%>
            <br/>
            Quantity: <%=order.getQuantity()%>
            <br/>
            Price: <%=order.getPrice()%>
            <br/>      
            Total: <%=order.getTotal()%>
            <br/>    
            Name: <%=order.getName()%>
            <br/>    
            Address: <%=order.getAddress()%>
            <br/>
            Phone: <%=order.getPhone()%>
            <br/>
            Email: <%=order.getEmail()%>
            <br/>
            Date: <%=order.getDate()%>
            <br/>
            <div class="small-container-et">
                <div class="row-et">
                    <div class="col-2-et2">
                        <form action="manager.jsp">
                            <input type="submit" value="Cancel"/>
                        </form>
                    </div>
                    <div class="col-2-et2">
                        <form action="delete?target=3&id=<%=order.getId()%>&shoe_id=<%=shoe_id%>&size=<%=size%>" method="POST">
                            <input type="submit" value="Delete"/>
                        </form>
                    </div>
                </div>
            </div>
            <%}%>
        </div>

        <div class="footer-et">
            <div class="container-et">
                <div class="row-et">
                    <div class="footer-et-col-2">
                        <img src="Image/Logo_2_resize.png">
                    </div>
                    <div class="footer-et-col-4">
                        <h3>Follow us</h3>
                        <ul>
                            <li>Facebook</li>
                            <li>Twitter</li>
                            <li>Instagram</li>
                            <li>Youtube</li>
                        </ul>
                    </div>
                </div>
                <hr>
                <p class="Copyright">Copyright 2020 - SneakersStore</p>
            </div>
        </div>

        <script>
            var MenuItems = document.getElementById("MenuItems");
            MenuItems.style.maxHeight = "0px";
            function menutoggleet() {
                if (MenuItems.style.maxHeight == "0px") {
                    MenuItems.style.maxHeight = "200px";
                } else {
                    MenuItems.style.maxHeight = "0px";
                }
            }
        </script>

    </body>
</html>
