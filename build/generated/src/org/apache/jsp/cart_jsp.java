package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.List;
import model.Item;
import model.Item;
import model.Cart;

public final class cart_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<head>\n");
      out.write("    <meta charset=\"UTF-8\">\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n");
      out.write("    <title>Detail Products - SneakersStore</title>\n");
      out.write("    <link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl\" crossorigin=\"anonymous\">\n");
      out.write("    <link rel=\"stylesheet\" href=\"style.css\">\n");
      out.write("    <link href=\"https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap\" rel=\"stylesheet\">\n");
      out.write("    <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\">\n");
      out.write("    <script src=\"https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js\" integrity=\"sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi\" crossorigin=\"anonymous\"></script>\n");
      out.write("    <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js\" integrity=\"sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG\" crossorigin=\"anonymous\"></script>\n");
      out.write("    ");
 session = request.getSession();
       Cart cart = (Cart)session.getAttribute("cart");
       List<Item> items = cart.getItems();
       int total = 0;
    
      out.write("\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("    <div class=\"header-et\">\n");
      out.write("        <div class=\"container-et\">\n");
      out.write("            <div class=\"navbar-et\">\n");
      out.write("                <div class=\"logo\">\n");
      out.write("                    <a href=\"index.html\">\n");
      out.write("                    <img src=\"Image/Logo_2.png\" width=\"150px\">\n");
      out.write("                    </a>\n");
      out.write("                </div>\n");
      out.write("                <nav class=\"nav-et\">\n");
      out.write("                    <ul id=\"MenuItems\">\n");
      out.write("                        <li><a href=\"index.html\">Home</a></li>\n");
      out.write("                        <li><a href=\"product.jsp?page=1\">Product</a></li>\n");
      out.write("                        <li><a href=\"\">About</a></li>\n");
      out.write("                        <li><a href=\"\">Contact</a></li>\n");
      out.write("                        <li><a href=\"account.jsp\">Account</a></li>\n");
      out.write("                    </ul>\n");
      out.write("                </nav>\n");
      out.write("                <a href=\"cart.jsp\">\n");
      out.write("                <img src=\"Image/cart.jpg\" width=\"30px\" height=\"30px\">\n");
      out.write("                </a>\n");
      out.write("                <img src=\"Image/menu.png\" class=\"menu-icon-et\" onclick=\"menutoggleet()\">\n");
      out.write("            </div>\n");
      out.write("        </div> \n");
      out.write("    </div>\n");
      out.write("    <!-- --------------- cart item ------------------ -->\n");
      out.write("    <div class=\"small-container-et cart-page\">\n");
      out.write("        <table class=\"table-et\">\n");
      out.write("            <tr>\n");
      out.write("                <th>Product</th>\n");
      out.write("                <th>Size</th>\n");
      out.write("                <th>Quantity</th>\n");
      out.write("                <th>Subtotal</th>\n");
      out.write("            </tr>\n");
      out.write("            ");
for(Item item : items){
                String[] shoe_image = item.getShoe().getShoe_image();
                total = item.getShoe().getPrice()*item.getQuantity();
            
      out.write("\n");
      out.write("            <tr>\n");
      out.write("                <td>\n");
      out.write("                    <div class=\"cart-info\">\n");
      out.write("                        <img src=");
      out.print(shoe_image[0]);
      out.write(">\n");
      out.write("                        <div>\n");
      out.write("                            <p>");
      out.print(item.getShoe().getShoe_name());
      out.write("</p>\n");
      out.write("                            <small>Price: ");
      out.print(item.getShoe().getPrice());
      out.write("₫</small>\n");
      out.write("                            <br>\n");
      out.write("                            <a href=\"\">Remove</a>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                </td>\n");
      out.write("                <td><p>");
      out.print(item.getSize());
      out.write("</p></td>\n");
      out.write("                <td><p>");
      out.print(item.getQuantity());
      out.write("</p></td>\n");
      out.write("                <td>");
      out.print(item.getShoe().getPrice()*item.getQuantity());
      out.write("₫</td>\n");
      out.write("            </tr>\n");
      out.write("            ");
}
      out.write("\n");
      out.write("        </table>\n");
      out.write("\n");
      out.write("        <div class=\"total-price\">\n");
      out.write("            <table>\n");
      out.write("                <tr>\n");
      out.write("                    <td>Subtotal</td>\n");
      out.write("                    <td>");
      out.print(total);
      out.write("₫</td>\n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                    <td>Ship</td>\n");
      out.write("                    <td>30.000₫</td>\n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                    <td>Total</td>\n");
      out.write("                    <td>");
      out.print(total+30000);
      out.write("₫</td>\n");
      out.write("                </tr>\n");
      out.write("            </table>\n");
      out.write("        </div>\n");
      out.write("    </div>\n");
      out.write("    \n");
      out.write("        <!-- ---------- footer -----------    -->\n");
      out.write("        <div class=\"footer-et\">\n");
      out.write("            <div class=\"container-et\">\n");
      out.write("                <div class=\"row-et\">\n");
      out.write("                    <div class=\"footer-et-col-2\">\n");
      out.write("                        <img src=\"Image/Logo_2_resize.png\">\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"footer-et-col-4\">\n");
      out.write("                        <h3>Follow us</h3>\n");
      out.write("                        <ul>\n");
      out.write("                            <li>Facebook</li>\n");
      out.write("                            <li>Twitter</li>\n");
      out.write("                            <li>Instagram</li>\n");
      out.write("                            <li>Youtube</li>\n");
      out.write("                        </ul>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("                <hr>\n");
      out.write("                <p class=\"Copyright\">Copyright 2020 - SneakersStore</p>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("    \n");
      out.write("    <script>\n");
      out.write("        var MenuItems = document.getElementById(\"MenuItems\");\n");
      out.write("        MenuItems.style.maxHeight = \"0px\";\n");
      out.write("        function menutoggleet(){\n");
      out.write("            if(MenuItems.style.maxHeight == \"0px\"){\n");
      out.write("                MenuItems.style.maxHeight = \"200px\";\n");
      out.write("            }else{\n");
      out.write("                MenuItems.style.maxHeight = \"0px\";\n");
      out.write("            }\n");
      out.write("        }\n");
      out.write("    </script>\n");
      out.write("\n");
      out.write("    </body>\n");
      out.write("    </html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
