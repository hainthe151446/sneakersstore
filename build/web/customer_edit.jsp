<%-- 
    Document   : customer_edit
    Created on : Mar 29, 2021, 11:18:51 PM
    Author     : nguye
--%>

<%@page import="model.Customer"%>
<%@page import="DAL.CustomerDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>SneakersStore</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <link rel="stylesheet" href="style.css">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>

    </head>
    <body>
        <div class="header-et">
            <div class="container-et">
                <div class="navbar-et">
                    <div class="logo">
                        <a href="index.html">
                            <img src="Image/Logo_2.png" width="150px">
                        </a>
                    </div>
                    <nav class="nav-et">
                        <ul id="MenuItems">
                            <li><a href="index.html">Home</a></li>
                            <li><a href="product.jsp?page=1">Product</a></li>
                            <li><a href="account.jsp">Account</a></li>
                        </ul>
                    </nav>
                    <a href="cart.jsp">
                        <img src="Image/cart.jpg" width="30px" height="30px">
                    </a>
                    <img src="Image/menu.png" class="menu-icon-et" onclick="menutoggleet()">
                </div>
            </div>
        </div>

        <div class="small-container-et" style="padding: 5%">
            <%
                int function = Integer.parseInt(String.valueOf(request.getAttribute("function")));
                if (function == 1) {
            %>
            <form action="add?target=2" method="POST">
                Name <input type="text" name="name" />
                <br/>
                Phone <input type="text" name="phone" />
                <br/>
                Email <input type="text" name="email" />
                <br/>
                Address <input type="text" name="address" />
                <br/>        
                Username <input type="text" name="username" />
                <br/>
                Password <input type="text" name="password" />
                <br/>
                <input type="submit" value="Add"/>
            </form>
            <%} else if (function == 2) {
                int id = Integer.parseInt(String.valueOf(request.getAttribute("customer_id")));
                CustomerDAO db = new CustomerDAO();
                Customer customer = db.getCustomerbyId(id);
            %>
            <form action="edit?target=2&id=<%=id%>" method="POST">
                Name <input type="text" name="name" value="<%= customer.getName()%>" />
                <br/>
                Phone <input type="text" name="phone" value="<%= customer.getPhone()%>"/>
                <br/>
                Email <input type="text" name="email" value="<%= customer.getEmail()%>"/>
                <br/>
                Address <input type="text" name="address" value="<%= customer.getAddress()%>"/>
                <br/>        
                Username <input type="text" name="username" value="<%= customer.getUsername()%>"/>
                <br/>
                Password <input type="text" name="password" value="<%= customer.getPassword()%>"/>
                <br/>
                <input type="submit" value="Save"/>
            </form>
            <%} else if (function == 3) {
                int id = Integer.parseInt(String.valueOf(request.getAttribute("customer_id")));
                CustomerDAO db = new CustomerDAO();
                Customer customer = db.getCustomerbyId(id);%>
            Name: <%= customer.getName()%>
            <br/>
            Phone: <%= customer.getPhone()%>
            <br/>
            Email: <%= customer.getEmail()%>
            <br/>
            Address: <%= customer.getAddress()%>
            <br/>        
            Username: <%= customer.getUsername()%>
            <br/>
            Password: <%= customer.getPassword()%>
            <br/>
            <div class="small-container-et">
                <div class="row-et">
                    <div class="col-2-et2">
                        <form action="manager.jsp">
                            <input type="submit" value="Cancel"/>
                        </form>
                    </div>
                    <div class="col-2-et2">
                        <form action="delete?target=2&id=<%=id%>" method="POST">
                            <input type="submit" value="Delete"/>
                        </form>
                    </div>
                </div>
            </div>
            <%}%>
        </div>

        <div class="footer-et">
            <div class="container-et">
                <div class="row-et">
                    <div class="footer-et-col-2">
                        <img src="Image/Logo_2_resize.png">
                    </div>
                    <div class="footer-et-col-4">
                        <h3>Follow us</h3>
                        <ul>
                            <li>Facebook</li>
                            <li>Twitter</li>
                            <li>Instagram</li>
                            <li>Youtube</li>
                        </ul>
                    </div>
                </div>
                <hr>
                <p class="Copyright">Copyright 2020 - SneakersStore</p>
            </div>
        </div>

        <script>
            var MenuItems = document.getElementById("MenuItems");
            MenuItems.style.maxHeight = "0px";
            function menutoggleet() {
                if (MenuItems.style.maxHeight == "0px") {
                    MenuItems.style.maxHeight = "200px";
                } else {
                    MenuItems.style.maxHeight = "0px";
                }
            }
        </script>

    </body>
</html>
