<%-- 
    Document   : manager
    Created on : Mar 29, 2021, 7:57:24 PM
    Author     : nguye
--%>

<%@page import="model.Quantity"%>
<%@page import="model.ShoeImage"%>
<%@page import="model.Order"%>
<%@page import="DAL.OrderDAO"%>
<%@page import="model.Customer"%>
<%@page import="DAL.CustomerDAO"%>
<%@page import="model.Shoe"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.ArrayList"%>
<%@page import="DAL.ShoeDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SneakersStore</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <%
        ShoeDAO shoedb = new ShoeDAO();
        ArrayList<Shoe> shoes = shoedb.getShoe();
        CustomerDAO customerdb = new CustomerDAO();
        ArrayList<Customer> customers = customerdb.getCustomer();
        OrderDAO orderdb = new OrderDAO();
        ArrayList<Order> orders = orderdb.getOrder();
    %>
</head>
<body>
    <div class="header-et">
        <div class="container-et">
            <div class="navbar-et">
                <div class="logo">
                    <a href="">
                        <img src="Image/Logo_2.png" width="150px">
                    </a>
                </div>
                <nav class="nav-et">
                    <ul id="MenuItems">
                        <li><a href="logout?type=admin">Log out</a></li>
                    </ul>
                </nav>
                <img src="Image/menu.png" class="menu-icon-et" onclick="menutoggleet()">
            </div>
        </div>
    </div>

    <section class="py-5 header">
        <div class="container py-4">

            <div class="row">
                <div class="col-md-3">
                    <!-- Tabs nav -->
                    <div class="nav flex-column nav-pills nav-pills-custom" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link mb-3 p-3 shadow active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                            <i class="fas fa-shoe-prints"></i>
                            <span class="font-weight-bold small text-uppercase">Shoe</span></a>

                        <a class="nav-link mb-3 p-3 shadow" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">
                            <i class="fas fa-id-card"></i>
                            <span class="font-weight-bold small text-uppercase">Customer</span></a>

                        <a class="nav-link mb-3 p-3 shadow" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">
                            <i class="fas fa-shopping-cart"></i>
                            <span class="font-weight-bold small text-uppercase">Order</span></a>


                    </div>
                </div>


                <div class="col-md-9">
                    <!-- Tabs content -->
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade shadow rounded bg-white show active p-5" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                            <h4 class="font-italic mb-4">Shoe Management</h4> 
                            <a href="add?target=1">Add</a>
                            <table border="1">
                                <tr>
                                    <td>
                                        ID
                                    </td>
                                    <td>
                                        Name
                                    </td>
                                    <td>
                                        Price
                                    </td>
                                    <td>
                                        Type
                                    </td>
                                    <td>
                                        Brand
                                    </td>
                                    <td>
                                        Description
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <% for (int i = 0; i < shoes.size(); i++) {
                                %>
                                <tr>
                                    <td>
                                        <%=shoes.get(i).getShoe_id()%>
                                    </td> 
                                    <td>
                                        <%=shoes.get(i).getShoe_name()%>
                                    </td>
                                    <td>
                                        <%=shoes.get(i).getPrice()%>
                                    </td>                
                                    <td>
                                        <%=shoes.get(i).getShoe_type()%>
                                    </td>
                                    <td>
                                        <%=shoes.get(i).getShoe_brand()%>
                                    </td> 
                                    <td>
                                        <%=shoes.get(i).getShoe_description()%>
                                    </td> 
                                    <td>   
                                        <a href="edit?id=<%=shoes.get(i).getShoe_id()%>&target=1">Edit</a>
                                        <a href="delete?id=<%=shoes.get(i).getShoe_id()%>&target=1">Delete</a>
                                    </td>
                                </tr>
                                <%}%>
                            </table>
                        </div>

                        <div class="tab-pane fade shadow rounded bg-white p-5" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                            <h4 class="font-italic mb-4">Customer Management</h4>
                            <a href="add?target=2">Add</a>
                            <table border="1">
                                <tr>
                                    <td>
                                        ID
                                    </td>
                                    <td>
                                        Name
                                    </td>
                                    <td>
                                        Phone
                                    </td>
                                    <td>
                                        Email
                                    </td>
                                    <td>
                                        Address
                                    </td>
                                    <td>
                                        Username
                                    </td>
                                    <td>
                                        Password
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <% for (int i = 0; i < customers.size(); i++) {
                                %>
                                <tr>
                                    <td>
                                        <%=customers.get(i).getId()%>
                                    </td> 
                                    <td>
                                        <%=customers.get(i).getName()%>
                                    </td>
                                    <td>
                                        <%=customers.get(i).getPhone()%>
                                    </td>                
                                    <td>
                                        <%=customers.get(i).getEmail()%>
                                    </td>
                                    <td>
                                        <%=customers.get(i).getAddress()%>
                                    </td> 
                                    <td>
                                        <%=customers.get(i).getUsername()%>
                                    </td> 
                                    <td>
                                        <%=customers.get(i).getPassword()%>
                                    </td> 
                                    <td>   
                                        <a href="edit?id=<%=customers.get(i).getId()%>&target=2">Edit</a>
                                        <a href="delete?id=<%=customers.get(i).getId()%>&target=2">Delete</a>
                                    </td>
                                </tr>
                                <%}%>
                            </table>
                        </div>

                        <div class="tab-pane fade shadow rounded bg-white p-5" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                            <h4 class="font-italic mb-4">Order Management</h4>
                            <a href="add?target=3">Add</a>
                            <table border="1">
                                <tr>
                                    <td>
                                        Order ID
                                    </td>
                                    <td>
                                        Customer ID
                                    </td>
                                    <td>
                                        Shoe ID
                                    </td>
                                    <td>
                                        Size
                                    </td>
                                    <td>
                                        Quantity
                                    </td>
                                    <td>
                                        Price
                                    </td>
                                    <td>
                                        Total
                                    </td>
                                    <td>
                                        Name
                                    </td>
                                    <td>
                                        Address
                                    </td>
                                    <td>
                                        Phone
                                    </td>
                                    <td>
                                        Email
                                    </td>
                                    <td>
                                        Date
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <% for (int i = 0; i < orders.size(); i++) {
                                %>
                                <tr>
                                    <td>
                                        <%=orders.get(i).getId()%>
                                    </td> 
                                    <td>
                                        <%=orders.get(i).getCustomer_id()%>
                                    </td>
                                    <td>
                                        <%=orders.get(i).getShoe_id()%>
                                    </td>                
                                    <td>
                                        <%=orders.get(i).getSize()%>
                                    </td>
                                    <td>
                                        <%=orders.get(i).getQuantity()%>
                                    </td> 
                                    <td>
                                        <%=orders.get(i).getPrice()%>
                                    </td> 
                                    <td>
                                        <%=orders.get(i).getTotal()%>
                                    </td>
                                    <td>
                                        <%=orders.get(i).getName()%>
                                    </td>
                                    <td>
                                        <%=orders.get(i).getAddress()%>
                                    </td>
                                    <td>
                                        <%=orders.get(i).getPhone()%>
                                    </td>
                                    <td>
                                        <%=orders.get(i).getEmail()%>
                                    </td>
                                    <td>
                                        <%=orders.get(i).getDate()%>
                                    </td>
                                    <td>   
                                        <a href="edit?id=<%=orders.get(i).getId()%>&target=3&shoe_id=<%=orders.get(i).getShoe_id()%>&size=<%=orders.get(i).getSize()%>">Edit</a>
                                        <a href="delete?id=<%=orders.get(i).getId()%>&target=3&shoe_id=<%=orders.get(i).getShoe_id()%>&size=<%=orders.get(i).getSize()%>">Delete</a>
                                    </td>
                                </tr>
                                <%}%>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
                            
    <div class="footer-et">
        <div class="container-et">
            <div class="row-et">
                <div class="footer-et-col-2">
                    <img src="Image/Logo_2_resize.png">
                </div>
                <div class="footer-et-col-4">
                    <h3>Follow us</h3>
                    <ul>
                        <li>Facebook</li>
                        <li>Twitter</li>
                        <li>Instagram</li>
                        <li>Youtube</li>
                    </ul>
                </div>
            </div>
            <hr>
            <p class="Copyright">Copyright 2020 - SneakersStore</p>
        </div>
    </div>

    <script>
        var MenuItems = document.getElementById("MenuItems");
        MenuItems.style.maxHeight = "0px";
        function menutoggleet() {
            if (MenuItems.style.maxHeight == "0px") {
                MenuItems.style.maxHeight = "200px";
            } else {
                MenuItems.style.maxHeight = "0px";
            }
        }
    </script>

</body>
</html>
