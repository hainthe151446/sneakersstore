<%-- 
    Document   : product
    Created on : Mar 23, 2021, 2:33:21 PM
    Author     : nguye
--%>

<%@page import="DAL.ShoeDAO"%>
<%@page import="model.Shoe"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>All Products - SneakersStore</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
    <%  ShoeDAO db = new ShoeDAO();
        ArrayList<Shoe> shoes = db.getShoe();
    %>
</head>
<body>
    <div class="header-et">
        <div class="container-et">
            <div class="navbar-et">
                <div class="logo">
                    <a href="index.html">
                        <img src="Image/Logo_2.png" width="150px">
                    </a>
                </div>
                <nav class="nav-et">
                    <ul id="MenuItems">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="product.jsp?page=1">Product</a></li>
                        <li><a href="account.jsp">Account</a></li>
                    </ul>
                </nav>
                <a href="cart.jsp">
                    <img src="Image/cart.jpg" width="30px" height="30px">
                </a>
                <img src="Image/menu.png" class="menu-icon-et" onclick="menutoggleet()">
            </div>
        </div>      
    </div>
    
    <div class="small-container-et">
        <div class="row-et row-2">
            <h2>All Products</h2>
        </div>


        <%
            String spageid = request.getParameter("page");
            int pageid = Integer.parseInt(spageid);
            int total = 8;
            int start = pageid * total - 7;
            int end = pageid * total;
            ArrayList<Shoe> shoespaging = db.getShoePaging(start, end);
        %>
        <div class="row-et">
            <%for (int i = 0; i < shoespaging.size(); i++) {
                    String[] shoe_image = shoespaging.get(i).getShoe_image();
            %>
            <div class="col-4-et">
                <a href="detail?id=<%=shoespaging.get(i).getShoe_id()%>">
                    <img src=<%=shoe_image[0]%>>
                </a>
                <a href="detail?id=<%=shoespaging.get(i).getShoe_id()%>" style="text-decoration: none;">
                    <h4><%=shoespaging.get(i).getShoe_name()%></h4>
                </a>
                <div class="rating">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
                <p><%=shoespaging.get(i).getPrice()%>₫</p>
            </div>
            <%}%>
        </div>
        <div class="page-btn">
            <% for (int i = 1; i <= (shoes.size() / 8) + 1; i++) {%>
            <a href="product.jsp?page=<%=i%>" style="text-decoration: none; color: #000000">
                <span><%=i%></span>
            </a>
            <%}%>
            <span>&#8594;</span>
        </div>
    </div>
    <div class="footer-et">
        <div class="container-et">
            <div class="row-et">
                <div class="footer-et-col-2">
                    <img src="Image/Logo_2_resize.png">
                </div>
                <div class="footer-et-col-4">
                    <h3>Follow us</h3>
                    <ul>
                        <li>Facebook</li>
                        <li>Twitter</li>
                        <li>Instagram</li>
                        <li>Youtube</li>
                    </ul>
                </div>
            </div>
            <hr>
            <p class="Copyright">Copyright 2020 - SneakersStore</p>
        </div>
    </div>

    <script>
        var MenuItems = document.getElementById("MenuItems");
        MenuItems.style.maxHeight = "0px";
        function menutoggleet() {
            if (MenuItems.style.maxHeight == "0px") {
                MenuItems.style.maxHeight = "200px";
            } else {
                MenuItems.style.maxHeight = "0px";
            }
        }
    </script>

</body>
</html>