<%-- 
    Document   : cart
    Created on : Mar 23, 2021, 4:41:16 PM
    Author     : nguye
--%>

<%@page import="java.util.List"%>
<%@page import="model.Item"%>
<%@page import="model.Item"%>
<%@page import="model.Cart"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Detail Products - SneakersStore</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
    <%int total = 0;%>
</head>
<body>
    <div class="header-et">
        <div class="container-et">
            <div class="navbar-et">
                <div class="logo">
                    <a href="index.html">
                        <img src="Image/Logo_2.png" width="150px">
                    </a>
                </div>
                <nav class="nav-et">
                    <ul id="MenuItems">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="product.jsp?page=1">Product</a></li>
                        <li><a href="account.jsp">Account</a></li>
                    </ul>
                </nav>
                <a href="cart.jsp">
                    <img src="Image/cart.jpg" width="30px" height="30px">
                </a>
                <img src="Image/menu.png" class="menu-icon-et" onclick="menutoggleet()">
            </div>
        </div> 
    </div>
    <!-- --------------- cart item ------------------ -->
    <%  session = request.getSession();
        if (session.getAttribute("cart") == null) {%>
    <div class="small-container-et cart-page">
        <table class="table-et">
            <tr>
                <th>Product</th>
                <th>Size</th>
                <th>Quantity</th>
                <th>Subtotal</th>
            </tr>
            <%for (int i = 0; i < 20; i++) {
            %>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <%}%>
        </table>
    </div>
    <%} else {
        Cart cart = (Cart) session.getAttribute("cart");
        List<Item> items = cart.getItems();
    %>
    <div class="small-container-et cart-page">
        <table class="table-et">
            <tr>
                <th>Product</th>
                <th>Size</th>
                <th>Quantity</th>
                <th>Subtotal</th>
            </tr>
            <%for (Item item : items) {
                    String[] shoe_image = item.getShoe().getShoe_image();
                    total += item.getShoe().getPrice() * item.getQuantity();
            %>
            <tr>
                <td>
                    <div class="cart-info">
                        <img src=<%=shoe_image[0]%>>
                        <div>
                            <p><%=item.getShoe().getShoe_name()%></p>
                            <small>Price: <%=item.getShoe().getPrice()%>₫</small>
                            <br>
                            <a href="removeItem?id=<%=item.getShoe().getShoe_id()%>&size=<%=item.getSize()%>" onclick="if (confirm('Are you sure you want to delete?'))
                                            form.action = 'removeItem?id=${item.getShoe().getShoe_id()}&size=${item.getSize()}';
                                        else
                                            return false;">Remove</a>
                        </div>
                        
                    </div>
                </td>
                <td><p><%=item.getSize()%></p></td>
                <td><p><%=item.getQuantity()%></p></td>
                <td><%=item.getShoe().getPrice() * item.getQuantity()%>₫</td>
            </tr>
            <%}%>
        </table>

        <div class="total-price">
            <table>
                <tr>
                    <td>Subtotal</td>
                    <td><%=total%>₫</td>
                    <% session.setAttribute("total", total);%>
                </tr>
                <tr>
                    <td>Ship</td>
                    <td>30.000₫</td>
                </tr>
                <tr>
                    <td>Total</td>
                    <td><%=total + 30000%>₫</td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <a href="checkout.jsp">
                            <button type="submit" class="btn-et" style="width: 100%">GO TO CHECKOUT</button>
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <%}%>



    <!-- ---------- footer -----------    -->
    <div class="footer-et">
        <div class="container-et">
            <div class="row-et">
                <div class="footer-et-col-2">
                    <img src="Image/Logo_2_resize.png">
                </div>
                <div class="footer-et-col-4">
                    <h3>Follow us</h3>
                    <ul>
                        <li>Facebook</li>
                        <li>Twitter</li>
                        <li>Instagram</li>
                        <li>Youtube</li>
                    </ul>
                </div>
            </div>
            <hr>
            <p class="Copyright">Copyright 2020 - SneakersStore</p>
        </div>
    </div>

    <script>
        var MenuItems = document.getElementById("MenuItems");
        MenuItems.style.maxHeight = "0px";
        function menutoggleet() {
            if (MenuItems.style.maxHeight == "0px") {
                MenuItems.style.maxHeight = "200px";
            } else {
                MenuItems.style.maxHeight = "0px";
            }
        }
    </script>

</body>
</html>
