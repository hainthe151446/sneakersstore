<%-- 
    Document   : account
    Created on : Mar 23, 2021, 5:13:53 PM
    Author     : nguye
--%>

<%@page import="model.Admin"%>
<%@page import="model.Customer"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Detail Products - SneakersStore</title>
    <link href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css'>
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js'></script>
    <link href='https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/3.6.95/css/materialdesignicons.css'>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
    <%
        session = request.getSession();
        Admin admin = (Admin) session.getAttribute("admin");
        Customer customer = (Customer) session.getAttribute("customer");
    %>
</head>
<body>
    <div class="header-et">
        <div class="container-et">
            <div class="navbar-et">
                <div class="logo">
                    <a href="index.html">
                        <img src="Image/Logo_2.png" width="150px">
                    </a>
                </div>
                <nav class="nav-et">
                    <ul id="MenuItems">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="product.jsp?page=1">Product</a></li>
                        <li><a href="account.jsp">Account</a></li>
                    </ul>
                </nav>
                <a href="cart.jsp">
                    <img src="Image/cart.jpg" width="30px" height="30px">
                </a>
                <img src="Image/menu.png" class="menu-icon-et" onclick="menutoggleet()">
            </div>
        </div> 
    </div>

    <!-- --------------account------------- -->
    <%if (customer != null) {%>
    <div class="page-content page-container" id="page-content">
        <div class="padding">
            <div class="row container d-flex justify-content-center">
                <div class="col-xl-6 col-md-12">
                    <div class="card user-card-full">
                        <div class="row m-l-0 m-r-0">
                            <div class="col-sm-4 bg-c-lite-green user-profile">
                                <div class="card-block text-center text-white">
                                    <div class="m-b-25"> <img src="https://img.icons8.com/bubbles/100/000000/user.png" class="img-radius" alt="User-Profile-Image"> </div>
                                    <h6 class="f-w-600"><%= customer.getName()%></h6>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="card-block">
                                    <h6 class="m-b-20 p-b-5 b-b-default f-w-600">Information</h6>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <p class="m-b-10 f-w-600">Email</p>
                                            <h6 class="text-muted f-w-400"><%= customer.getEmail()%></h6>
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="m-b-10 f-w-600">Phone</p>
                                            <h6 class="text-muted f-w-400"><%= customer.getPhone()%></h6>
                                        </div>
                                    </div>
                                    <h6 class="m-b-20 m-t-40 p-b-5 b-b-default f-w-600"></h6>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <p class="m-b-10 f-w-600">Address</p>
                                            <h6 class="text-muted f-w-400"><%= customer.getAddress()%></h6>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <form action="logout">
                        <input type="hidden" value="customer" name="type">
                        <button type="submit" class="btn-et" style="width: 12%; font-size: 125%">Logout</button>
                    </form>     
                </div>
            </div>
        </div>
    </div>
    <%} else {%>
    <div class="account-page">
        <div class="container-et">
            <div class="row-et">
                <div class="col-2-et">
                    <img src="Image/Logo_1.png" alt="">
                </div>

                <div class="col-2-et">
                    <div class="form-container" style="height: 500px;">
                        <div class="form-btn">
                            <span onclick="Registor()">Register</span>
                            <span onclick="Login()">Login</span>
                            <hr id="Indicator">
                        </div>
                        <form action="addCustomer" method="POST" id="RegForm" style="left: -300px;">
                            <input type="text" placeholder="Name" name="name">
                            <input type="text" placeholder="Phone" name="phone">
                            <input type="text" placeholder="Address" name="address">
                            <input type="text" placeholder="Email" name="email">
                            <input type="text" placeholder="Username" name="username">
                            <input type="password" placeholder="Password" name="password">
                            <button type="submit" class="btn-et">Register</button>
                        </form>
                        <form action="login" method="POST" id="LoginForm" style="left: 0;">
                            <input type="text" placeholder="Username" name="username">
                            <input type="password" placeholder="Password" name="password">
                            <button type="submit" class="btn-et">Login</button>
                            <a href="">Forgot password</a>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <%}%>



    <!-- ---------- footer -----------    -->
    <div class="footer-et">
        <div class="container-et">
            <div class="row-et">
                <div class="footer-et-col-2">
                    <img src="Image/Logo_2_resize.png">
                </div>
                <div class="footer-et-col-4">
                    <h3>Follow us</h3>
                    <ul>
                        <li>Facebook</li>
                        <li>Twitter</li>
                        <li>Instagram</li>
                        <li>Youtube</li>
                    </ul>
                </div>
            </div>
            <hr>
            <p class="Copyright">Copyright 2020 - SneakersStore</p>
        </div>
    </div>

    <script>
        var MenuItems = document.getElementById("MenuItems");
        MenuItems.style.maxHeight = "0px";
        function menutoggleet() {
            if (MenuItems.style.maxHeight == "0px") {
                MenuItems.style.maxHeight = "200px";
            } else {
                MenuItems.style.maxHeight = "0px";
            }
        }
    </script>

    <!-- ---------- js for toggle form -----------    -->

    <script>
        var LoginForm = document.getElementById("LoginForm");
        var RegForm = document.getElementById("RegForm");
        var Indicator = document.getElementById("Indicator");
        function Registor() {
            RegForm.style.transform = "translateX(300px)";
            LoginForm.style.transform = "translateX(300px)"
            Indicator.style.transform = "translateX(0px)"

        }
        function Login() {
            RegForm.style.transform = "translateX(0px)";
            LoginForm.style.transform = "translateX(0px)"
            Indicator.style.transform = "translateX(100px)"

        }
    </script>

</body>
</html>
