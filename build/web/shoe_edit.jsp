<%-- 
    Document   : shoe_edit
    Created on : Mar 29, 2021, 10:55:57 PM
    Author     : nguye
--%>

<%@page import="DAL.ShoeDAO"%>
<%@page import="model.Shoe"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>SneakersStore</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <link rel="stylesheet" href="style.css">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>

    </head>
    <body>
        <div class="header-et">
            <div class="container-et">
                <div class="navbar-et">
                    <div class="logo">
                        <a href="index.html">
                            <img src="Image/Logo_2.png" width="150px">
                        </a>
                    </div>
                    <nav class="nav-et">
                        <ul id="MenuItems">
                            <li><a href="index.html">Home</a></li>
                            <li><a href="product.jsp?page=1">Product</a></li>
                            <li><a href="account.jsp">Account</a></li>
                        </ul>
                    </nav>
                    <a href="cart.jsp">
                        <img src="Image/cart.jpg" width="30px" height="30px">
                    </a>
                    <img src="Image/menu.png" class="menu-icon-et" onclick="menutoggleet()">
                </div>
            </div>
        </div>

        <div class="small-container-et" style="padding: 5%">
            <%
                int function = Integer.parseInt(String.valueOf(request.getAttribute("function")));
                if (function == 1) {
            %>
            <form action="add?target=1" method="POST">
                Shoe ID <input type="text" name="id" />
                <br/>
                Name <input type="text" name="name" />
                <br/>
                Price <input type="text" name="price" />
                <br/>
                Type <input type="text" name="type" />
                <br/>
                Brand <input type="text" name="brand" />
                <br/>        
                Description <input type="text" name="description" />
                <br/>
                Size 7 <input type="text" name="size7" />
                <br/>
                Size 7.5 <input type="text" name="size75" />
                <br/>
                Size 8 <input type="text" name="size8" />
                <br/>
                Size 8.5 <input type="text" name="size85" />
                <br/>
                Size 9 <input type="text" name="size9" />
                <br/>
                Size 9.5 <input type="text" name="size95" />
                <br/>
                Image 1 <input type="text" name="image1" />
                <br/>
                Image 2 <input type="text" name="image2" />
                <br/>
                Image 3 <input type="text" name="image3" />
                <br/>
                Image 4 <input type="text" name="image4" />
                <br/>
                <input type="submit" value="Save"/>
            </form>
            <%} else if (function == 2) {
                String id = String.valueOf(request.getAttribute("shoe_id"));
                ShoeDAO db = new ShoeDAO();
                Shoe shoe = db.getShoe(id);
            %>
            <form action="edit?target=1&id=<%=id%>" method="POST">
                Shoe ID <input type="text" name="id" value="<%= shoe.getShoe_id()%>" />
                <br/>
                Name <input type="text" name="name" value="<%= shoe.getShoe_name()%>"/>
                <br/>
                Price <input type="text" name="price" value="<%= shoe.getPrice()%>"/>
                <br/>
                Type <input type="text" name="type" value="<%= shoe.getShoe_type()%>"/>
                <br/>
                Brand <input type="text" name="brand" value="<%= shoe.getShoe_brand()%>"/>
                <br/>        
                Description <input type="text" name="description" value="<%= shoe.getShoe_description()%>"/>
                <br/>
                Size 7 <input type="text" name="size7" value="<%= shoe.getQuantity().getSize7()%>"/>
                <br/>
                Size 7.5 <input type="text" name="size75" value="<%= shoe.getQuantity().getSize75()%>"/>
                <br/>
                Size 8 <input type="text" name="size8" value="<%= shoe.getQuantity().getSize8()%>"/>
                <br/>
                Size 8.5 <input type="text" name="size85" value="<%= shoe.getQuantity().getSize85()%>"/>
                <br/>
                Size 9 <input type="text" name="size9" value="<%= shoe.getQuantity().getSize9()%>"/>
                <br/>
                Size 9.5 <input type="text" name="size95" value="<%= shoe.getQuantity().getSize95()%>"/>
                <br/>
                Image 1 <input type="text" name="image1" value="<%= shoe.getShoe_image()[0]%>"/>
                <br/>
                Image 2 <input type="text" name="image2" value="<%= shoe.getShoe_image()[1]%>"/>
                <br/>
                Image 3 <input type="text" name="image3" value="<%= shoe.getShoe_image()[2]%>"/>
                <br/>
                Image 4 <input type="text" name="image4" value="<%= shoe.getShoe_image()[3]%>"/>
                <br/>
                <input type="submit" value="Save"/>
            </form>
            <%} else if (function == 3) {
                String id = String.valueOf(request.getAttribute("shoe_id"));
                ShoeDAO db = new ShoeDAO();
                Shoe shoe = db.getShoe(id);%>

            Shoe ID: <%= shoe.getShoe_id()%>
            <br/>
            Name: <%= shoe.getShoe_name()%>
            <br/>
            Price: <%= shoe.getPrice()%>
            <br/>
            Type: <%= shoe.getShoe_type()%>
            <br/>
            Brand: <%= shoe.getShoe_brand()%>
            <br/>        
            Description: <%= shoe.getShoe_description()%>
            <br/>
            Size 7: <%= shoe.getQuantity().getSize7()%>
            <br/>
            Size 7.5: <%= shoe.getQuantity().getSize75()%>
            <br/>
            Size 8: <%= shoe.getQuantity().getSize8()%>
            <br/>
            Size 8.5: <%= shoe.getQuantity().getSize85()%>
            <br/>
            Size 9: <%= shoe.getQuantity().getSize9()%>
            <br/>
            Size 9.5: <%= shoe.getQuantity().getSize95()%>
            <br/>
            Image 1: <%= shoe.getShoe_image()[0]%>
            <br/>
            Image 2: <%= shoe.getShoe_image()[1]%>
            <br/>
            Image 3: <%= shoe.getShoe_image()[2]%>
            <br/>
            Image 4: <%= shoe.getShoe_image()[3]%>
            <br/>
            <div class="small-container-et">
                <div class="row-et">
                    <div class="col-2-et2">
                        <form action="manager.jsp">
                            <input type="submit" value="Cancel"/>
                        </form>
                    </div>
                    <div class="col-2-et2">
                        <form action="delete?target=1&id=<%=id%>" method="POST">
                            <input type="submit" value="Delete"/>
                        </form>
                    </div>
                </div>
            </div>

            <%}%>
        </div>
        <div class="footer-et">
            <div class="container-et">
                <div class="row-et">
                    <div class="footer-et-col-2">
                        <img src="Image/Logo_2_resize.png">
                    </div>
                    <div class="footer-et-col-4">
                        <h3>Follow us</h3>
                        <ul>
                            <li>Facebook</li>
                            <li>Twitter</li>
                            <li>Instagram</li>
                            <li>Youtube</li>
                        </ul>
                    </div>
                </div>
                <hr>
                <p class="Copyright">Copyright 2020 - SneakersStore</p>
            </div>
        </div>

        <script>
            var MenuItems = document.getElementById("MenuItems");
            MenuItems.style.maxHeight = "0px";
            function menutoggleet() {
                if (MenuItems.style.maxHeight == "0px") {
                    MenuItems.style.maxHeight = "200px";
                } else {
                    MenuItems.style.maxHeight = "0px";
                }
            }
        </script>

    </body>
</html>

