/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import DAL.CustomerDAO;
import DAL.OrderDAO;
import DAL.ShoeDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Shoe;

/**
 *
 * @author nguye
 */
public class DeleteServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DeleteServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DeleteServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int target = Integer.parseInt(request.getParameter("target"));
        int function = 3;
        request.setAttribute("function", function);
        if (target == 1) {
            String id = request.getParameter("id");
            request.setAttribute("shoe_id", id);
            request.getRequestDispatcher("shoe_edit.jsp").forward(request, response);
        } else if (target == 2) {
            int id = Integer.parseInt(request.getParameter("id"));
            request.setAttribute("customer_id", id);
            request.getRequestDispatcher("customer_edit.jsp").forward(request, response);
        } else if (target == 3) {
            int id = Integer.parseInt(request.getParameter("id"));
            String shoe_id = request.getParameter("shoe_id");
            float size = Float.parseFloat(request.getParameter("size"));
            request.setAttribute("order_id", id);
            request.setAttribute("shoe_id", shoe_id);
            request.setAttribute("size", size);
            request.getRequestDispatcher("order_edit.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int target = Integer.parseInt(request.getParameter("target"));
        if (target == 1) {
            String shoe_id = request.getParameter("id");
            ShoeDAO db = new ShoeDAO();
            db.DeleteShoe(shoe_id);
            response.sendRedirect("manager.jsp");
        } else if (target == 2) {
            int id = Integer.parseInt(request.getParameter("id"));
            CustomerDAO db = new CustomerDAO();
            db.DeleteCustomer(id);
            response.sendRedirect("manager.jsp");
        } else if (target == 3) {
            int order_id = Integer.parseInt(request.getParameter("id"));
            String shoe_id = request.getParameter("shoe_id");
            float size = Float.parseFloat(request.getParameter("size"));
            OrderDAO db = new OrderDAO();
            db.DeleteOrder(order_id,shoe_id,size);
            response.sendRedirect("manager.jsp");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
