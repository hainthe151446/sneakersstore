/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import DAL.CustomerDAO;
import DAL.OrderDAO;
import DAL.ShoeDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Customer;
import model.Order;
import model.Quantity;
import model.Shoe;

/**
 *
 * @author nguye
 */
public class AddServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int target = Integer.parseInt(request.getParameter("target"));
        int function = 1;
        request.setAttribute("function", function);
        if (target == 1) {
            request.getRequestDispatcher("shoe_edit.jsp").forward(request, response);
        } else if (target == 2) {
            request.getRequestDispatcher("customer_edit.jsp").forward(request, response);
        } else if (target == 3) {
            request.getRequestDispatcher("order_edit.jsp").forward(request, response);

        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int target = Integer.parseInt(request.getParameter("target"));
        if (target == 1) {
            String shoe_id = request.getParameter("id");
            String shoe_name = request.getParameter("name");
            int price = Integer.parseInt(request.getParameter("price"));
            String type = request.getParameter("type");
            String brand = request.getParameter("brand");
            String description = request.getParameter("description");
            int size7 = Integer.parseInt(request.getParameter("size7"));
            int size75 = Integer.parseInt(request.getParameter("size75"));
            int size8 = Integer.parseInt(request.getParameter("size8"));
            int size85 = Integer.parseInt(request.getParameter("size85"));
            int size9 = Integer.parseInt(request.getParameter("size9"));
            int size95 = Integer.parseInt(request.getParameter("size95"));
            Quantity quatity = new Quantity(shoe_id, size7, size75, size8, size85, size9, size95);
            String image1 = request.getParameter("image1");
            String image2 = request.getParameter("image2");
            String image3 = request.getParameter("image3");
            String image4 = request.getParameter("image4");
            String[] shoe_image = {image1, image2, image3, image4};
            Shoe shoe = new Shoe(shoe_id, shoe_name, price, quatity, type, brand, description, shoe_image);
            ShoeDAO db = new ShoeDAO();
            db.insertShoe(shoe);
            response.sendRedirect("manager.jsp");
        } else if (target == 2) {
            String name = request.getParameter("name");
            String phone = request.getParameter("phone");
            String email = request.getParameter("email");
            String address = request.getParameter("address");
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            Customer c = new Customer();
            c.setName(name);
            c.setPhone(phone);
            c.setEmail(email);
            c.setAddress(address);
            c.setUsername(username);
            c.setPassword(password);
            CustomerDAO db = new CustomerDAO();
            db.insertCustomer(c);
            response.sendRedirect("manager.jsp");
        } else if (target == 3) {
            int order_id = Integer.parseInt(request.getParameter("order_id"));
            int customer_id = Integer.parseInt(request.getParameter("customer_id"));
            String shoe_id = request.getParameter("shoe_id");
            float size = Float.parseFloat(request.getParameter("size"));
            int quantity = Integer.parseInt(request.getParameter("quantity"));
            int price = Integer.parseInt(request.getParameter("price"));
            int total = Integer.parseInt(request.getParameter("total"));
            String name = request.getParameter("name");
            String address = request.getParameter("address");
            String phone = request.getParameter("phone");
            String email = request.getParameter("email");
            Date date = Date.valueOf(request.getParameter("date"));
            Order order = new Order(order_id, customer_id, shoe_id, size, quantity, price, total, name, address, phone, email, date);
            OrderDAO db = new OrderDAO();
            db.addOrder(order);
            response.sendRedirect("manager.jsp");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
