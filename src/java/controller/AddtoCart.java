/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import DAL.ShoeDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Cart;
import model.Item;
import model.Shoe;

/**
 *
 * @author nguye
 */
@WebServlet(name = "AddtoCart", urlPatterns = {"/AddtoCart"})
public class AddtoCart extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddtoCart</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddtoCart at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int quantity = 1;
        String id;
        if (request.getParameter("shoe_id") != null) {
            id = request.getParameter("shoe_id");
            Float size = Float.parseFloat(request.getParameter("selectSize"));
            ShoeDAO db = new ShoeDAO();
            Shoe shoe = db.getShoe(id);
            String id2 = null;
            if (shoe != null) {
                if (request.getParameter("quantity") != null) {
                    quantity = Integer.parseInt(request.getParameter("quantity"));
                }
                HttpSession session = request.getSession();
                if (session.getAttribute("cart") == null) {
                    Cart cart = new Cart();
                    List<Item> items = new ArrayList<Item>();
                    Item item = new Item();
                    item.setQuantity(quantity);
                    item.setSize(size);
                    item.setShoe(shoe);
                    item.setPrice(shoe.getPrice());
                    items.add(item);
                    cart.setItems(items);
                    session.setAttribute("cart", cart);
                } else {
                    Cart cart = (Cart) session.getAttribute("cart");
                    List<Item> items = cart.getItems();
                    boolean check = false;
                    boolean check2 = false;
                    for (Item item : items) {
                        if (item.getShoe().getShoe_id().equals(shoe.getShoe_id())) {
                            id2 = item.getShoe().getShoe_id();
                            if (item.getSize() == size) {
                                check2 = true;
                            }
                        }
                    }
                    if (id2 != null) {
                        if (check2 == true) {
                            for (Item item : items) {
                                if (item.getSize() == size) {
                                    item.setQuantity(item.getQuantity() + quantity);
                                    check = true;
                                }
                            }
                        } else {
                            Item item2 = new Item();
                            item2.setQuantity(quantity);
                            item2.setShoe(shoe);
                            item2.setSize(size);
                            item2.setPrice(shoe.getPrice());
                            items.add(item2);
                            session.setAttribute("cart", cart);
                            response.sendRedirect(request.getContextPath() + "/detail?id=" + id);
                            return;
                        }
                    }
                    if (check == false) {
                        Item item = new Item();
                        item.setQuantity(quantity);
                        item.setShoe(shoe);
                        item.setSize(size);
                        item.setPrice(shoe.getPrice());
                        items.add(item);
                    }
                    session.setAttribute("cart", cart);
                }
            }
            response.sendRedirect(request.getContextPath() + "/detail?id=" + id);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
