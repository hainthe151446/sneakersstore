/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author nguye
 */
public class Shoe {

    private String shoe_id;
    private String shoe_name;
    private int price;
    private Quantity quantity;
    private String shoe_type;
    private String shoe_brand;
    private String shoe_description;
    private String[] shoe_image;

    public Shoe() {
    }
    
    public Shoe(String shoe_id, String shoe_name, int price, Quantity quantity, String shoe_type, String shoe_brand, String shoe_description, String[] shoe_image) {
        this.shoe_id = shoe_id;
        this.shoe_name = shoe_name;
        this.price = price;
        this.quantity = quantity;
        this.shoe_type = shoe_type;
        this.shoe_brand = shoe_brand;
        this.shoe_description = shoe_description;
        this.shoe_image = shoe_image;
    }
    
    

    public String getShoe_id() {
        return shoe_id;
    }

    public void setShoe_id(String shoe_id) {
        this.shoe_id = shoe_id;
    }

    public String getShoe_name() {
        return shoe_name;
    }

    public void setShoe_name(String shoe_name) {
        this.shoe_name = shoe_name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Quantity getQuantity() {
        return quantity;
    }

    public void setQuantity(Quantity quantity) {
        this.quantity = quantity;
    }

    public String getShoe_type() {
        return shoe_type;
    }

    public void setShoe_type(String shoe_type) {
        this.shoe_type = shoe_type;
    }

    public String getShoe_brand() {
        return shoe_brand;
    }

    public void setShoe_brand(String shoe_brand) {
        this.shoe_brand = shoe_brand;
    }

    public String getShoe_description() {
        return shoe_description;
    }

    public void setShoe_description(String shoe_description) {
        this.shoe_description = shoe_description;
    }

    public String[] getShoe_image() {
        return shoe_image;
    }

    public void setShoe_image(String[] shoe_image) {
        this.shoe_image = shoe_image;
    }

    

    
}
