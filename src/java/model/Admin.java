/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author nguye
 */
public class Admin {

    private String admin_name;
    private String admin_pasword;

    public Admin() {
    }

    public Admin(String admin_name, String admin_pasword) {
        this.admin_name = admin_name;
        this.admin_pasword = admin_pasword;
    }

    public String getAdmin_name() {
        return admin_name;
    }

    public void setAdmin_name(String admin_name) {
        this.admin_name = admin_name;
    }

    public String getAdmin_pasword() {
        return admin_pasword;
    }

    public void setAdmin_pasword(String admin_pasword) {
        this.admin_pasword = admin_pasword;
    }
    
}
