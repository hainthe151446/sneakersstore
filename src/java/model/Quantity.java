/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author nguye
 */
public class Quantity {
    private String shoe_id;
    private int size7, size75, size8, size85, size9, size95;

    public Quantity() {
    }

    public Quantity(String shoe_id, int size7, int size75, int size8, int size85, int size9, int size95) {
        this.shoe_id = shoe_id;
        this.size7 = size7;
        this.size75 = size75;
        this.size8 = size8;
        this.size85 = size85;
        this.size9 = size9;
        this.size95 = size95;
    }

    public String getShoe_id() {
        return shoe_id;
    }

    public void setShoe_id(String shoe_id) {
        this.shoe_id = shoe_id;
    }

    public int getSize7() {
        return size7;
    }

    public void setSize7(int size7) {
        this.size7 = size7;
    }

    public int getSize75() {
        return size75;
    }

    public void setSize75(int size75) {
        this.size75 = size75;
    }

    public int getSize8() {
        return size8;
    }

    public void setSize8(int size8) {
        this.size8 = size8;
    }

    public int getSize85() {
        return size85;
    }

    public void setSize85(int size85) {
        this.size85 = size85;
    }

    public int getSize9() {
        return size9;
    }

    public void setSize9(int size9) {
        this.size9 = size9;
    }

    public int getSize95() {
        return size95;
    }

    public void setSize95(int size95) {
        this.size95 = size95;
    }
    
}
