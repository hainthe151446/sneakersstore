/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Admin;
import model.Customer;

/**
 *
 * @author nguye
 */
public class CustomerDAO extends BaseDAO<Customer> {

    public Customer getCustomerbyUsernamePassword(String username, String password) {
        ArrayList<Customer> customers = new ArrayList<>();
        try {
            String sql = "SELECT [customer_id]\n"
                    + "      ,[customer_name]\n"
                    + "      ,[phone_number]\n"
                    + "      ,[customer_email]\n"
                    + "      ,[customer_address]\n"
                    + "      ,[customer_username]\n"
                    + "      ,[customer_password]\n"
                    + "  FROM [project].[dbo].[Customer]";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Customer c = new Customer();
                c.setId(rs.getInt("customer_id"));
                c.setName(rs.getString("customer_name"));
                c.setPhone(rs.getString("phone_number"));
                c.setEmail(rs.getString("customer_email"));
                c.setAddress(rs.getString("customer_address"));
                c.setUsername(rs.getString("customer_username"));
                c.setPassword(rs.getString("customer_password"));
                customers.add(c);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (Customer c : customers) {
            if (username.equals(c.getUsername()) && password.equals(c.getPassword())) {
                Customer customer = new Customer();
                customer.setId(c.getId());
                customer.setName(c.getName());
                customer.setPhone(c.getPhone());
                customer.setEmail(c.getEmail());
                customer.setAddress(c.getAddress());
                customer.setUsername(username);
                customer.setPassword(password);
                return customer;
            }
        }
        return null;
    }

    public Customer getCustomerbyId(int id) {
        ArrayList<Customer> customers = new ArrayList<>();
        try {
            String sql = "SELECT [customer_id]\n"
                    + "      ,[customer_name]\n"
                    + "      ,[phone_number]\n"
                    + "      ,[customer_email]\n"
                    + "      ,[customer_address]\n"
                    + "      ,[customer_username]\n"
                    + "      ,[customer_password]\n"
                    + "  FROM [project].[dbo].[Customer]";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Customer c = new Customer();
                c.setId(rs.getInt("customer_id"));
                c.setName(rs.getString("customer_name"));
                c.setPhone(rs.getString("phone_number"));
                c.setEmail(rs.getString("customer_email"));
                c.setAddress(rs.getString("customer_address"));
                c.setUsername(rs.getString("customer_username"));
                c.setPassword(rs.getString("customer_password"));
                customers.add(c);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (Customer c : customers) {
            if (id == c.getId()) {
                Customer customer = new Customer();
                customer.setId(c.getId());
                customer.setName(c.getName());
                customer.setPhone(c.getPhone());
                customer.setEmail(c.getEmail());
                customer.setAddress(c.getAddress());
                customer.setUsername(c.getUsername());
                customer.setPassword(c.getPassword());
                return customer;
            }
        }
        return null;
    }

    public ArrayList<Customer> getCustomer() {
        ArrayList<Customer> customers = new ArrayList<>();
        try {
            String sql = "SELECT [customer_id]\n"
                    + "      ,[customer_name]\n"
                    + "      ,[phone_number]\n"
                    + "      ,[customer_email]\n"
                    + "      ,[customer_address]\n"
                    + "      ,[customer_username]\n"
                    + "      ,[customer_password]\n"
                    + "  FROM [project].[dbo].[Customer]";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Customer c = new Customer();
                c.setId(rs.getInt("customer_id"));
                c.setName(rs.getString("customer_name"));
                c.setPhone(rs.getString("phone_number"));
                c.setEmail(rs.getString("customer_email"));
                c.setAddress(rs.getString("customer_address"));
                c.setUsername(rs.getString("customer_username"));
                c.setPassword(rs.getString("customer_password"));
                customers.add(c);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return customers;
    }

    public int getIdbyUsername(String username) {
        int id = 0;
        try {
            String sql = "SELECT [customer_id]\n"
                    + "      ,[customer_name]\n"
                    + "      ,[phone_number]\n"
                    + "      ,[customer_email]\n"
                    + "      ,[customer_address]\n"
                    + "      ,[customer_username]\n"
                    + "      ,[customer_password]\n"
                    + "  FROM [project].[dbo].[Customer]";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                id = rs.getInt("customer_id");
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return id;
    }

    public void insertCustomer(Customer c) {
        try {
            String sql = "INSERT INTO [Customer]\n"
                    + "           ([customer_name]\n"
                    + "      ,[phone_number]\n"
                    + "      ,[customer_email]\n"
                    + "      ,[customer_address]\n"
                    + "      ,[customer_username]\n"
                    + "      ,[customer_password])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?)";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, c.getName());
            statement.setString(2, c.getPhone());
            statement.setString(3, c.getEmail());
            statement.setString(4, c.getAddress());
            statement.setString(5, c.getUsername());
            statement.setString(6, c.getPassword());
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void EditCustomer(Customer c) {
        try {
            String sql = "UPDATE [Customer]\n"
                    + "SET [customer_name] = ?\n"
                    + ",[phone_number] = ?\n"
                    + ",[customer_email] = ?\n"
                    + ",[customer_address] = ?\n"
                    + ", [customer_username] = ?\n"
                    + ", [customer_password] = ?\n"
                    + "WHERE customer_id = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, c.getName());
            statement.setString(2, c.getPhone());
            statement.setString(3, c.getEmail());
            statement.setString(4, c.getAddress());
            statement.setString(5, c.getUsername());
            statement.setString(6, c.getPassword());
            statement.setInt(7, c.getId());
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void DeleteCustomer(int id) {
        try {
            String sql = "DELETE Customer WHERE customer_id = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            statement.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Admin getAdminbyUsernamePassword(String username, String password) {
        ArrayList<Admin> admins = new ArrayList<>();
        try {
            String sql = "SELECT [admin_id]\n"
                    + "      ,[admin_name]\n"
                    + "      ,[admin_password]\n"
                    + "  FROM [project].[dbo].[Admin]";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Admin a = new Admin();
                a.setAdmin_name(rs.getString("admin_name"));
                a.setAdmin_pasword(rs.getString("admin_password"));
                admins.add(a);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (Admin a : admins) {
            if (username.equals(a.getAdmin_name()) && password.equals(a.getAdmin_pasword())) {
                Admin admin = new Admin();
                admin.setAdmin_name(a.getAdmin_name());
                admin.setAdmin_pasword(a.getAdmin_pasword());
                return admin;
            }
        }
        return null;
    }

    @Override
    public ArrayList<Customer> getShoe() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<Customer> getShoePaging(int start, int end, String value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
