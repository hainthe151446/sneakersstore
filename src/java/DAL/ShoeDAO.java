/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Quantity;
import model.Shoe;
import model.ShoeImage;

/**
 *
 * @author nguye
 */
public class ShoeDAO extends BaseDAO<Shoe> {

    @Override
    public ArrayList<Shoe> getShoe() {
        ArrayList<Shoe> shoes = new ArrayList<>();
        try {
            String sql = "SELECT Shoe.[shoe_id], shoe_name,\n"
                    + "price,\n"
                    + "shoe_type, shoe_brand,\n"
                    + "shoe_description,\n"
                    + "[shoe_image1],\n"
                    + "[shoe_image2],\n"
                    + "[shoe_image3],\n"
                    + "[shoe_image4]\n"
                    + "  FROM Shoe, [project].[dbo].[productImage]\n"
                    + "  WHERE Shoe.shoe_id = [project].[dbo].[productImage].shoe_id";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Shoe s = new Shoe();
                s.setShoe_id(rs.getString("shoe_id"));
                s.setShoe_name(rs.getString("shoe_name"));
                s.setPrice(rs.getInt("price"));
                s.setShoe_type(rs.getString("shoe_type"));
                s.setShoe_brand(rs.getString("shoe_brand"));
                s.setShoe_description(rs.getString("shoe_description"));
                String[] shoe_image = {rs.getString("shoe_image1"), rs.getString("shoe_image2"), rs.getString("shoe_image3"), rs.getString("shoe_image4")};
                s.setShoe_image(shoe_image);
                shoes.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ShoeDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return shoes;
    }

    public Shoe getShoe(String id) {
        try {
            String sql = "SELECT Shoe.[shoe_id], shoe_name,\n"
                    + "price,\n"
                    + "shoe_type, shoe_brand,\n"
                    + "shoe_description,\n"
                    + "[shoe_image1],\n"
                    + "[shoe_image2],\n"
                    + "[shoe_image3],\n"
                    + "[shoe_image4],\n"
                    + "size7, size75, size8, size85, size9, size95\n"
                    + "  FROM Shoe, [project].[dbo].[productImage], [project].[dbo].[Quantity]\n"
                    + "  WHERE Shoe.shoe_id = [project].[dbo].[productImage].shoe_id AND Shoe.shoe_id = [project].[dbo].[Quantity].shoe_id AND Shoe.shoe_id = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                Shoe s = new Shoe();
                Quantity q = new Quantity();
                s.setShoe_id(rs.getString("shoe_id"));
                s.setShoe_name(rs.getString("shoe_name"));
                s.setPrice(rs.getInt("price"));
                q.setSize7(rs.getInt("size7"));
                q.setSize75(rs.getInt("size75"));
                q.setSize8(rs.getInt("size8"));
                q.setSize85(rs.getInt("size85"));
                q.setSize9(rs.getInt("size9"));
                q.setSize95(rs.getInt("size95"));
                s.setQuantity(q);
                s.setShoe_type(rs.getString("shoe_type"));
                s.setShoe_brand(rs.getString("shoe_brand"));
                s.setShoe_description(rs.getString("shoe_description"));
                String[] shoe_image = {rs.getString("shoe_image1"), rs.getString("shoe_image2"), rs.getString("shoe_image3"), rs.getString("shoe_image4")};
                s.setShoe_image(shoe_image);
                return s;
            }

        } catch (SQLException ex) {
            Logger.getLogger(ShoeDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    @Override
        public ArrayList<Shoe> getShoePaging(int start, int end, String value) {
            return null;
        }

    public ArrayList<Shoe> getShoePaging(int start, int end) {
        ArrayList<Shoe> shoes = new ArrayList<Shoe>();
        try {
            String sql = "SELECT ShoePaging.[shoe_id], shoe_name,\n"
                    + "price,\n"
                    + "size7, size75, size8, size85, size9, size95, \n"
                    + "shoe_type, shoe_brand,\n"
                    + "shoe_description,\n"
                    + "[shoe_image1],\n"
                    + "[shoe_image2],\n"
                    + "[shoe_image3],\n"
                    + "[shoe_image4]\n"
                    + "FROM\n"
                    + "(SELECT ROW_NUMBER() OVER (ORDER BY s.price ASC) as rownum, s.[shoe_id], shoe_name,\n"
                    + "price,\n"
                    + "size7, size75, size8, size85, size9, size95, \n"
                    + "shoe_type, shoe_brand,\n"
                    + "shoe_description,\n"
                    + "[shoe_image1],\n"
                    + "[shoe_image2],\n"
                    + "[shoe_image3],\n"
                    + "[shoe_image4]\n"
                    + "FROM Shoe s, [project].[dbo].[productImage], [project].[dbo].Quantity\n"
                    + "WHERE s.shoe_id = [project].[dbo].[productImage].shoe_id AND s.shoe_id = [project].[dbo].Quantity.shoe_id) as ShoePaging\n"
                    + "WHERE\n"
                    + "rownum >= ? AND rownum <= ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, start);
            statement.setInt(2, end);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Shoe s = new Shoe();
                s.setShoe_id(rs.getString("shoe_id"));
                s.setShoe_name(rs.getString("shoe_name"));
                s.setPrice(rs.getInt("price"));
                Quantity quantity = new Quantity(rs.getString("shoe_id"), rs.getInt("size7"), rs.getInt("size75"), rs.getInt("size8"), rs.getInt("size85"), rs.getInt("size9"), rs.getInt("size95"));
                s.setQuantity(quantity);
                s.setShoe_type(rs.getString("shoe_type"));
                s.setShoe_brand(rs.getString("shoe_brand"));
                s.setShoe_description(rs.getString("shoe_description"));
                String[] shoe_image = {rs.getString("shoe_image1"), rs.getString("shoe_image2"), rs.getString("shoe_image3"), rs.getString("shoe_image4")};
                s.setShoe_image(shoe_image);
                shoes.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ShoeDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return shoes;
    }

    public ArrayList<Shoe> getShoeSameBrand(String brand) {
        ArrayList<Shoe> shoes = new ArrayList<>();
        try {
            String sql = "SELECT Shoe.[shoe_id], shoe_name,\n"
                    + "price,\n"
                    + "shoe_type, shoe_brand,\n"
                    + "shoe_description,\n"
                    + "[shoe_image1],\n"
                    + "[shoe_image2],\n"
                    + "[shoe_image3],\n"
                    + "[shoe_image4]\n"
                    + "  FROM Shoe, [project].[dbo].[productImage]\n"
                    + "  WHERE Shoe.shoe_id = [project].[dbo].[productImage].shoe_id AND shoe_brand=?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, brand);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Shoe s = new Shoe();
                s.setShoe_id(rs.getString("shoe_id"));
                s.setShoe_name(rs.getString("shoe_name"));
                s.setPrice(rs.getInt("price"));
                s.setShoe_type(rs.getString("shoe_type"));
                s.setShoe_brand(rs.getString("shoe_brand"));
                s.setShoe_description(rs.getString("shoe_description"));
                String[] shoe_image = {rs.getString("shoe_image1"), rs.getString("shoe_image2"), rs.getString("shoe_image3"), rs.getString("shoe_image4")};
                s.setShoe_image(shoe_image);
                shoes.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ShoeDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return shoes;
    }

    public ArrayList<ShoeImage> getShoeImage() {
        ArrayList<ShoeImage> images = new ArrayList<>();
        try {
            String sql = "SELECT [shoe_id]\n"
                    + "      ,[shoe_image1]\n"
                    + "      ,[shoe_image2]\n"
                    + "      ,[shoe_image3]\n"
                    + "      ,[shoe_image4]\n"
                    + "  FROM [project].[dbo].[productImage]";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                ShoeImage i = new ShoeImage();
                i.setShoe_id(rs.getString("shoe_id"));
                i.setImage1(rs.getString("shoe_image1"));
                i.setImage2(rs.getString("shoe_image2"));
                i.setImage3(rs.getString("shoe_image3"));
                i.setImage4(rs.getString("shoe_image4"));
                images.add(i);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ShoeDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return images;
    }

    public ArrayList<Quantity> getQuantity() {
        ArrayList<Quantity> quantities = new ArrayList<>();
        try {
            String sql = "SELECT [shoe_id]\n"
                    + "      ,[size7]\n"
                    + "      ,[size75]\n"
                    + "      ,[size8]\n"
                    + "      ,[size85]\n"
                    + "      ,[size9]\n"
                    + "      ,[size95]\n"
                    + "  FROM [project].[dbo].[Quantity]";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Quantity q = new Quantity();
                q.setShoe_id(rs.getString("shoe_id"));
                q.setSize7(rs.getInt("size7"));
                q.setSize75(rs.getInt("size75"));
                q.setSize8(rs.getInt("size8"));
                q.setSize85(rs.getInt("size85"));
                q.setSize9(rs.getInt("size9"));
                q.setSize95(rs.getInt("size95"));
                quantities.add(q);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ShoeDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return quantities;
    }

    public void insertShoe(Shoe s) {
        try {
            String sql = "INSERT INTO Shoe(shoe_id, shoe_name, "
                    + "price, shoe_type, shoe_brand, shoe_description) "
                    + "VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?)";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, s.getShoe_id());
            statement.setString(2, s.getShoe_name());
            statement.setInt(3, s.getPrice());
            statement.setString(4, s.getShoe_type());
            statement.setString(5, s.getShoe_brand());
            statement.setString(6, s.getShoe_description());
            statement.executeUpdate();

            sql = "INSERT INTO productImage(shoe_id, shoe_image1, shoe_image2, shoe_image3, shoe_image4) VALUES \n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?)";
            statement = connection.prepareStatement(sql);
            statement.setString(1, s.getShoe_id());
            statement.setString(2, s.getShoe_image()[0]);
            statement.setString(3, s.getShoe_image()[0]);
            statement.setString(4, s.getShoe_image()[0]);
            statement.setString(5, s.getShoe_image()[0]);
            statement.executeUpdate();

            sql = "INSERT INTO Quantity(shoe_id, size7, size75, size8, size85, size9, size95) VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?)";
            statement = connection.prepareStatement(sql);
            statement.setString(1, s.getShoe_id());
            statement.setInt(2, s.getQuantity().getSize7());
            statement.setInt(3, s.getQuantity().getSize75());
            statement.setInt(4, s.getQuantity().getSize8());
            statement.setInt(5, s.getQuantity().getSize85());
            statement.setInt(6, s.getQuantity().getSize9());
            statement.setInt(7, s.getQuantity().getSize95());
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ShoeDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void EditShoe(Shoe s) {
        try {
            String sql = "UPDATE [Shoe]\n"
                    + "SET shoe_id = ?\n"
                    + ",shoe_name = ?\n"
                    + ",price = ?\n"
                    + ",shoe_type = ?\n"
                    + ",shoe_brand = ?\n"
                    + ", shoe_description = ?\n"
                    + "WHERE shoe_id = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, s.getShoe_id());
            statement.setString(2, s.getShoe_name());
            statement.setInt(3, s.getPrice());
            statement.setString(4, s.getShoe_type());
            statement.setString(5, s.getShoe_brand());
            statement.setString(6, s.getShoe_description());
            statement.setString(7, s.getShoe_id());
            statement.executeUpdate();

            sql = "UPDATE [productImage]\n"
                    + "SET shoe_id = ?\n"
                    + ",shoe_image1 = ?\n"
                    + ",shoe_image2 = ?\n"
                    + ",shoe_image3 = ?\n"
                    + ",shoe_image4 = ?\n"
                    + "WHERE shoe_id = ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1, s.getShoe_id());
            statement.setString(2, s.getShoe_image()[0]);
            statement.setString(3, s.getShoe_image()[0]);
            statement.setString(4, s.getShoe_image()[0]);
            statement.setString(5, s.getShoe_image()[0]);
            statement.setString(6, s.getShoe_id());
            statement.executeUpdate();

            sql = "UPDATE [Quantity]\n"
                    + "SET shoe_id = ?\n"
                    + ",size7 = ?\n"
                    + ",size75 = ?\n"
                    + ",size8 = ?\n"
                    + ",size85 = ?\n"
                    + ",size9 = ?\n"
                    + ",size95 = ?\n"
                    + "WHERE shoe_id = ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1, s.getShoe_id());
            statement.setInt(2, s.getQuantity().getSize7());
            statement.setInt(3, s.getQuantity().getSize75());
            statement.setInt(4, s.getQuantity().getSize8());
            statement.setInt(5, s.getQuantity().getSize85());
            statement.setInt(6, s.getQuantity().getSize9());
            statement.setInt(7, s.getQuantity().getSize95());
            statement.setString(8, s.getShoe_id());
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ShoeDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void DeleteShoe(String id) {
        try {
            String sql = "DELETE productImage WHERE shoe_id=?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement = connection.prepareStatement(sql);
            statement.setString(1, id);
            statement.executeUpdate();

            sql = "DELETE Quantity WHERE shoe_id=?";
            statement = connection.prepareStatement(sql);
            statement.setString(1, id);
            statement.executeUpdate();

            sql = "DELETE Shoe WHERE shoe_id=?";
            statement = connection.prepareStatement(sql);
            statement.setString(1, id);
            statement.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(ShoeDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
