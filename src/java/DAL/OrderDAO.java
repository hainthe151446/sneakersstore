/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Order;

/**
 *
 * @author nguye
 */
public class OrderDAO extends BaseDAO<Order> {

    public ArrayList<Order> getOrder() {
        ArrayList<Order> orders = new ArrayList<>();
        try {
            String sql = "SELECT [order_id]\n"
                    + "      ,[customer_id]\n"
                    + "      ,[shoe_id]\n"
                    + "      ,[size]\n"
                    + "      ,[quantity]\n"
                    + "      ,[price]\n"
                    + "      ,[total]\n"
                    + "      ,[order_name]\n"
                    + "      ,[order_address]\n"
                    + "      ,[order_phone]\n"
                    + "      ,[order_email]\n"
                    + "      ,[order_date]\n"
                    + "  FROM [project].[dbo].[Orders]\n"
                    + "  ORDER BY [order_id] asc";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Order o = new Order();
                o.setId(rs.getInt("order_id"));
                o.setCustomer_id(rs.getInt("customer_id"));
                o.setShoe_id(rs.getString("shoe_id"));
                o.setSize(rs.getFloat("size"));
                o.setQuantity(rs.getInt("quantity"));
                o.setPrice(rs.getInt("price"));
                o.setTotal(rs.getInt("total"));
                o.setName(rs.getString("order_name"));
                o.setAddress(rs.getString("order_address"));
                o.setPhone(rs.getString("order_phone"));
                o.setEmail(rs.getString("order_email"));
                o.setDate(rs.getDate("order_date"));
                orders.add(o);
            }
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return orders;
    }

    public Order getOrderbyId(int id, String shoe_id, float size) {
        try {
            String sql = "SELECT [order_id]\n"
                    + "      ,[customer_id]\n"
                    + "      ,[shoe_id]\n"
                    + "      ,[size]\n"
                    + "      ,[quantity]\n"
                    + "      ,[price]\n"
                    + "      ,[total]\n"
                    + "      ,[order_name]\n"
                    + "      ,[order_address]\n"
                    + "      ,[order_phone]\n"
                    + "      ,[order_email]\n"
                    + "      ,[order_date]\n"
                    + "  FROM [project].[dbo].[Orders]\n"
                    + "  WHERE [order_id] = ? AND [shoe_id] = ? AND [size] = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            statement.setString(2, shoe_id);
            statement.setFloat(3, size);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Order o = new Order();
                o.setId(rs.getInt("order_id"));
                o.setCustomer_id(rs.getInt("customer_id"));
                o.setShoe_id(rs.getString("shoe_id"));
                o.setSize(rs.getFloat("size"));
                o.setQuantity(rs.getInt("quantity"));
                o.setPrice(rs.getInt("price"));
                o.setTotal(rs.getInt("total"));
                o.setName(rs.getString("order_name"));
                o.setAddress(rs.getString("order_address"));
                o.setPhone(rs.getString("order_phone"));
                o.setEmail(rs.getString("order_email"));
                o.setDate(rs.getDate("order_date"));
                return o;
            }
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int getOrderId() {
        int id = 0;
        try {
            String sql = "SELECT TOP 1 [order_id]\n"
                    + "FROM [project].[dbo].[Orders]\n"
                    + "ORDER BY order_id desc;";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                id = rs.getInt("order_id");
            }
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return id;
    }

    public void addOrder(Order o) {
        try {
            String sql = "INSERT INTO [Orders]\n"
                    + "           ([order_id]\n"
                    + "      ,[customer_id]\n"
                    + "      ,[shoe_id]\n"
                    + "      ,[size]\n"
                    + "      ,[quantity]\n"
                    + "      ,[price]\n"
                    + "      ,[total]\n"
                    + "      ,[order_name]\n"
                    + "      ,[order_address]\n"
                    + "      ,[order_phone]\n"
                    + "      ,[order_email]\n"
                    + "      ,[order_date])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?)";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, o.getId());
            if (o.getCustomer_id() == 0) {
                statement.setNull(2, Types.INTEGER);
            } else {
                statement.setInt(2, o.getCustomer_id());
            }
            statement.setString(3, o.getShoe_id());
            statement.setFloat(4, o.getSize());
            statement.setInt(5, o.getQuantity());
            statement.setInt(6, o.getPrice());
            statement.setInt(7, o.getTotal());
            statement.setString(8, o.getName());
            statement.setString(9, o.getAddress());
            statement.setString(10, o.getPhone());
            statement.setString(11, o.getEmail());
            statement.setDate(12, o.getDate());
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void EditOrder(Order o, int order_id, String shoe_id, float size) {
        try {
            String sql = "UPDATE Orders\n"
                    + "SET order_id = ?\n"
                    + ",customer_id = ?\n"
                    + ",shoe_id = ?\n"
                    + ",size = ?\n"
                    + ",quantity = ?\n"
                    + ",price = ?\n"
                    + ",total = ?\n"
                    + ",order_name = ?\n"
                    + ",order_address = ?\n"
                    + ",order_phone = ?\n"
                    + ",order_email = ?\n"
                    + ",order_date = ?\n"
                    + "WHERE [order_id] = ? AND [shoe_id] = ? AND [size] = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, o.getId());
            if (o.getCustomer_id() == 0) {
                statement.setNull(2, Types.INTEGER);
            } else {
                statement.setInt(2, o.getCustomer_id());
            }
            statement.setString(3, o.getShoe_id());
            statement.setFloat(4, o.getSize());
            statement.setInt(5, o.getQuantity());
            statement.setInt(6, o.getPrice());
            statement.setInt(7, o.getTotal());
            statement.setString(8, o.getName());
            statement.setString(9, o.getAddress());
            statement.setString(10, o.getPhone());
            statement.setString(11, o.getEmail());
            statement.setDate(12, o.getDate());
            statement.setInt(13, o.getId());
            statement.setString(14, shoe_id);
            statement.setFloat(15, size);
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void DeleteOrder(int order_id, String shoe_is, float size) {
        try {
            String sql = "DELETE Orders WHERE order_id = ? AND shoe_id=? AND size = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement = connection.prepareStatement(sql);
            statement.setInt(1, order_id);
            statement.setString(2, shoe_is);
            statement.setFloat(3, size);
            statement.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public ArrayList<Order> getShoe() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<Order> getShoePaging(int start, int end, String value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
